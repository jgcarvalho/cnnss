import numpy as np
import cupy as cp
import chainer
from chainer import cuda, Function, gradient_check, report, training, utils, Variable
from chainer import datasets, iterators, optimizers, serializers, initializers
from chainer import Link, Chain, ChainList
import chainer.functions as F
import chainer.links as L
from chainer.training import extensions
import load_data
from sys import argv

HIDDEN = int(argv[1])

class ModelHK(chainer.Chain):
    def __init(self):
        w = initializers.GlorotNormal()
        super(ModelHK, self).__init__()
        with self.init_scope():
            self.conv01 = L.Convolution2D(
                in_channels=22, out_channels=HIDDEN, ksize=(17,1), stride=1,pad=(8,0), initialW=w)
            self.conv02 = L.Convolution2D(
                in_channels=HIDDEN, out_channels=3, ksize=(1,1), pad=(0,0), initialW=w)
            
    def __call__(self, x):
        h = F.sigmoid(self.conv01(x))
        return self.conv02(h)

model = ModelHK()

train_dataset = load_data.SSDataset('/home/jgcarvalho/cnnss/chainer/data/train')
validation_dataset = load_data.SSDataset('/home/jgcarvalho/cnnss/chainer/data/validation')
test_dataset = load_data.SSDataset('/home/jgcarvalho/cnnss/chainer/data/test')


train_iter = iterators.SerialIterator(train_dataset, batch_size=100, shuffle=True)
validation_iter = iterators.SerialIterator(validation_dataset, batch_size=100, repeat=False, shuffle=False)
test_iter = iterators.SerialIterator(test_dataset, batch_size=100, repeat=False, shuffle=False)


optimizer = optimizers.Adam()
optimizer.setup(model)

def loss(x, y):
    F.softmax_cross_entropy(x, y, normalize=True)

updater = training.StandardUpdater(train_iter, optimizer, loss_func=loss)
trainer = training.Trainer(updater, (1000, 'epoch'), out='result')
# trainer.extend(extensions.Evaluator(validation_iter, model))
trainer.extend(extensions.LogReport())
# trainer.extend(extensions.PrintReport(['epoch', 'main/accuracy', 'validation/main/accuracy']))
# trainer.extend(extensions.ProgressBar())
trainer.run()  