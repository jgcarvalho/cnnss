import torch
import matplotlib.pyplot as plt
import seaborn
import numpy as np
from scipy.stats import rankdata
import math

params = torch.load("./resnet_21_8.pth")

feat = params['feat.conv.weight'].cpu().numpy()

x = feat[:,1:21,0,0]

def ranks(x):
    a = np.zeros((20,20,len(x)))
    for i in range(len(x)):
        r = rankdata(x[i],method='dense')
        for u in range(len(r)):
            for v in range(len(r)):
                a[u,v,i] = abs(r[u]-r[v])

    b = np.zeros((20,20))
    for i in range(20):
        for j in range(20):
            b[i,j] = np.median(a[i,j])


    return b
r = ranks(x)
print(r)
# print(x[0])
# print(rankdata(x[0],method='dense'))
# for i in params.keys():
#     print(i)
# x = np.transpose(feat[:,1:21,0,0])
# x_norm = (((x - x.min(0)) / x.ptp(0))-0.5)*2.0
# # seaborn.clustermap(feat[:,1:21,0,0], row_cluster=True, method='average', metric='euclidean')
seaborn.clustermap(np.transpose(r))
seaborn.clustermap(feat[:,1:21,0,0], z_score=0)
# seaborn.heatmap(r)
plt.show()