import numpy as np
from glob import glob
import os
from torch.utils.data import Dataset
import torch

class SSDataset(Dataset):
    def __init__(self, root):
        self.root_inputs = root+'/inputs/'
        self.labels = glob(root+'/labels/*')
    
    def __getitem__(self,index):
        path_label = self.labels[index] 
        bn = os.path.basename(path_label).split('_')[0]
        path_input = self.root_inputs + bn
        # return path_input, path_label
        return torch.load(path_input), torch.load(path_label)
    
    def __len__(self):
        return len(self.labels)


def main():
    db = SSDataset('./data/train')
    print(db.labels)
    for i in db:
        print(i)


if __name__ == '__main__':
    main()
