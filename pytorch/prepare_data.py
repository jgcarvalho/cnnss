import numpy as np
from glob import glob
import os
from torch.utils.data import Dataset
import torch


# aa symbols: '>' start aa sequence (n-terminal),
#             '<' end aa sequence (c-terminal) 
AA = ['>', 'A', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'K', 'L',
      'M', 'N', 'P', 'Q', 'R', 'S', 'T', 'V', 'Y', 'W', '<']

# ss symbols: '?' undefined or none ss, this symbol will be 
# ignored by loss function
SS = ['C', 'H', 'E', '?']

# PAD = 1

INPUT_CODE = len(AA)
LABEL_CODE = len(SS)

MAX_LENGTH = 3000
NTERM_PAD = 500
CTERM_PAD = 500

# ROOT_DIR = "/home/jgcarvalho/RCNN-SS/cntk/data/chamel_data/"
ROOT_DIR = "/home/jgcarvalho/cnnss/data/all50_data/"
SEQ_DIR = ROOT_DIR + "/seq/"
DSSP_DIR = ROOT_DIR + "/dssp/"
STRIDE_DIR = ROOT_DIR + "/stride/"
KAKSI_DIR = ROOT_DIR + "/kaksi/"
PROSS_DIR = ROOT_DIR + "/pross/"
ALL3_DIR = ROOT_DIR + "/all3/"

def encode_aa(seq):
    code = np.zeros((INPUT_CODE, MAX_LENGTH+NTERM_PAD+CTERM_PAD, 1))
    # code[AA.index('>'), 0, 0] = 1
    # code[AA.index('<'), len(seq)+1, 0] = 1
    for i in range(NTERM_PAD):
        code[AA.index('>'), i, 0] = 1

    for i, aa in enumerate(seq):
        code[AA.index(aa), i+NTERM_PAD, 0] = 1
    
    for i in range(NTERM_PAD+len(seq), len(code)):
        code[AA.index('<'), i, 0] = 1

    return torch.FloatTensor(code)

def encode_ss(seq):
    code = np.ones((MAX_LENGTH+NTERM_PAD+CTERM_PAD, 1), dtype=np.int) * SS.index('?')
    for i, ss in enumerate(seq):
        code[i+NTERM_PAD, 0] = SS.index(ss)

    return torch.LongTensor(code)

def read_file(fname,ss=False):
    f_ = open(fname)
    f_.readline()
    seq = f_.readline()
    # print(seq)
    f_.close()
    if ss:
        return encode_ss(seq)
    else:
        return encode_aa(seq)

def read_ss(fname):
    return read_file(fname,ss=True)

def read_aa(fname):
    return read_file(fname,ss=False)

def gen_dataset(low, high, mode='train'):
    file_names = [os.path.basename(x) for x in glob(SEQ_DIR + "*")][low:high]

    for fn in file_names:
        seq = read_aa(SEQ_DIR + fn)
        file_seq = './data/{}/inputs/{}'.format(mode,fn)
        torch.save(seq, file_seq)


        ss = read_ss(ALL3_DIR + fn)
        file_label = './data/{}/labels_all3/{}'.format(mode,fn)
        torch.save(ss, file_label)

        ss = read_ss(DSSP_DIR + fn)
        file_label = './data/{}/labels_dssp/{}'.format(mode,fn)
        torch.save(ss, file_label)

        ss = read_ss(STRIDE_DIR + fn)
        file_label = './data/{}/labels_stride/{}'.format(mode,fn)
        torch.save(ss, file_label)

        ss = read_ss(KAKSI_DIR + fn)
        file_label = './data/{}/labels_kaksi/{}'.format(mode,fn)
        torch.save(ss, file_label)

        ss = read_ss(PROSS_DIR + fn)
        file_label = './data/{}/labels_pross/{}'.format(mode,fn)
        torch.save(ss, file_label)

def main():
    gen_dataset(0,5000)
    gen_dataset(5000,5500,mode='validation')
    gen_dataset(5500,6700,mode='test')
    # print(encode_aa("AAAWWWW"))
    # print(encode_aa("AAAWWWW").shape)
    # print(encode_ss("HHEECC??"))
    # print(encode_ss("HHEECC??").shape)
    # dataset = gen_dataset(0,3)
    # print(dataset[0][0].shape)
    # print(dataset[1][0].shape)
    # print_data_cntk(dataset[0], dataset[1])

if __name__ == '__main__':
    main()
