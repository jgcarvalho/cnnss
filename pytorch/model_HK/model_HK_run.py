import numpy as np
import random
import torch 
from torch.autograd import Variable
from torch import nn
from torch.nn import init
from torch.utils.data import TensorDataset, DataLoader
from torch.optim import lr_scheduler
from sys import path, argv 
path.append('../')
import load_data


HIDDEN = int(argv[1])
TEST_DB = argv[2]
FILE_MODEL = './param_HK_64.pth'

AA = ['>', 'A', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'K', 'L',
      'M', 'N', 'P', 'Q', 'R', 'S', 'T', 'V', 'Y', 'W', '<']
SS = ['C', 'H', 'E', '?']

class Model(nn.Module):
    def __init__(self):
        super(Model,self).__init__()
        self.conv01 = nn.Conv2d(22, HIDDEN, (17,1), padding=(8,0))
        self.sigmoid01 = nn.Sigmoid()
        self.conv02 = nn.Conv2d(HIDDEN,3,(1,1), padding=(0,0))

    def forward(self,x):
        x = self.conv01(x)
        x = self.sigmoid01(x)
        x = self.conv02(x)
        return x

def weights_init(m):
    if isinstance(m, nn.Conv2d):
        init.xavier_normal(m.weight.data)
        # init.kaiming_normal(m.bias.data)

test_dataset = load_data.SSDataset('/tmp/data/test', TEST_DB)
test_loader = DataLoader(test_dataset,batch_size=1,shuffle=False)

model = Model()
model = model.cuda()
model.load_state_dict(torch.load(FILE_MODEL))
model.eval()

# TEST 
def test(test_loader):
    criterion = torch.nn.CrossEntropyLoss(size_average=True,ignore_index=3)
    test_loss = 0.0
    test_class_acc = 0.0
    q_c = 0.0
    q_h = 0.0
    q_e = 0.0

    i = 0
    for x_batch,y_batch in test_loader:
        x,y = Variable(x_batch).cuda(), Variable(y_batch, requires_grad=False).cuda()
        y_pred = model(x)
        # print(y_pred)
        loss = criterion(y_pred,y)

        mask = y.data < 3
        y_classes = y.data.index(mask)
        # print(mask)
        # print(y_classes)

        pred = y_pred.data.max(1)[1]
        pred_classes = pred.index(mask)
        # print(pred_classes)
            
        correct = pred_classes.eq(y_classes).cpu().sum()

        mask_c = y.data == 0
        mask_h = y.data == 1
        mask_e = y.data == 2
        y_c = y.data.index(mask_c)
        y_h = y.data.index(mask_h)
        y_e = y.data.index(mask_e)
        pred_c = pred.index(mask_c)
        pred_h = pred.index(mask_h)
        pred_e = pred.index(mask_e)
        correct_c = pred_c.eq(y_c).cpu().sum()
        correct_h = pred_h.eq(y_h).cpu().sum()
        correct_e = pred_e.eq(y_e).cpu().sum()

        q_c += correct_c/y_c.size()[0]
        q_h += correct_h/y_h.size()[0]
        q_e += correct_e/y_e.size()[0]


        test_loss += loss.data[0]
        test_class_acc += correct/y_classes.size()[0]
        i += 1
    print("TEST loss: %.3f accuracy: %.3f Qc: %.3f Qh: %.3f Qe: %.3f" % (test_loss/i, test_class_acc/i, q_c/i, q_h/i, q_e/i))

def apply(test_loader):
    for x_batch,y_batch in test_loader:
        x,y = Variable(x_batch).cuda(), Variable(y_batch, requires_grad=False).cuda()
        y_pred = model(x)

        _, idx_seq = torch.max(x.cpu(), 1)
        idx_ss = y.cpu()
        _, idx_pred = torch.max(y_pred.cpu(),1)
        seq = list(idx_seq.data.numpy().flatten())
        ss = list(idx_ss.data.numpy().flatten())
        pred = list(idx_pred.data.numpy().flatten())
        
        # print(x)
        # print(y_pred)
        # _, idx = torch.max(y_pred.cpu(),1)
        # index = 
        # res = ""
        seq_str = ""
        ss_str = ""
        pred_str = ""
        for i in range(len(seq)):
            if seq[i] != 0 and seq[i] != 21:
                seq_str += AA[seq[i]]
                ss_str += SS[ss[i]]
                pred_str += SS[pred[i]]
        print(seq_str)
        print(pred_str)
        print(ss_str)
        
        # print(list(ss.flatten()))
        # print(list(pred.flatten()))
            # print(SS[i])
        # print(indices)

print('TEST ALL3')
apply(test_loader)

# torch.save(model.state_dict(), FILE_MODEL)