import pandas as pd
import json

def main():
    conf = "data_cath95.json"
    c = ["training", "testing", "validation"]
    with open(conf, 'r') as json_file:
        jf = json.load(json_file)
        path = jf['path']
        files = []
        for a in c:
            files += jf[a]
        # print(files)
        json_file.close()
    # print(len(files))
    for ex in files:
    #     # print(i)
        df = pd.read_feather(path + ex['id'] + '.fth')
    #     # only use the specific chain 
        df = df.loc[df['res_chain'] == ex['chain']]
        print("{} {}".format(ex['id'], df['seq_res'].count()))
        if '?' in df['seq_res'].values:
            print("{}{}".format(ex['id'], ex['chain']))
        

if __name__ == '__main__':
    main()