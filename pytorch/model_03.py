import numpy as np
import random
import torch 
from torch.autograd import Variable
from torch import nn
from torch.nn import init
from torch.utils.data import TensorDataset, DataLoader
import data_02

class FeatModule(nn.Module):
    def __init__(self):
        super(FeatModule, self).__init__()
        self.conv = nn.Conv2d(22,16,(1,1))

    def forward(self,x):
        return self.conv(x)

class ConvModule(nn.Module):
    def __init__(self):
        super(ConvModule, self).__init__()
        self.conv01 = nn.Conv2d(16,16,(3,1), padding=(1,0))
        self.conv02 = nn.Conv2d(16,16,(3,1), padding=(1,0))
        self.bn = nn.InstanceNorm2d(16)
        self.relu = nn.ReLU()

    def forward(self,x):
        conv01 = self.conv01(x)
        relu01 = self.relu(conv01)
        conv02 = self.conv02(relu01)
        bn01 = self.bn(conv02)
        relu02 = self.relu(bn01)
        return relu02

# class FullModule(nn.Module):
#     def __init__(self):
#         super(FullModule, self).__init__()
#         self.m_in = ConvModule()
#         self.m_ou = ConvModule()

#     def forward(self,x):
#         return self.m_ou(self.m_in(x))

class ExitModule(nn.Module):
    def __init__(self):
        super(ExitModule, self).__init__()
        # self.drop = nn.Dropout2d(p=0.05)
        self.conv = nn.Conv2d(16,4,(1,1))
        self.relu = nn.ReLU()
        
    def forward(self,x):
        # return self.relu(self.conv(self.drop(x)))
        return self.relu(self.conv(x))

class Model(nn.Module):
    def __init__(self):
        super(Model,self).__init__()
        self.feat = FeatModule()
        self.m01 = ConvModule()
        self.e01 = ExitModule()
        # self.m02 = ConvModule()
        # self.e02 = ExitModule()
        # self.m03 = ConvModule()
        # self.e03 = ExitModule()
        # self.m04 = ConvModule()
        # self.e04 = ExitModule()
        # self.m05 = ConvModule()
        # self.e05 = ExitModule()
        # self.m06 = ConvModule()
        # self.e06 = ExitModule()
        # self.m07 = ConvModule()
        # self.e07 = ExitModule()

    def forward(self,x):
        feats = self.feat(x)
        m01 = self.m01(feats)
        # m02 = self.m02(m01)
        # m03 = self.m03(m02)
        # m04 = self.m04(m03)
        # m05 = self.m05(m04)
        # m06 = self.m06(m05)
        # m07 = self.m07(m06)
        # exit nodes
        e01 = self.e01(m01)
        # e02 = self.e02(m02)
        # e03 = self.e03(m03)
        # e04 = self.e04(m04)
        # e05 = self.e05(m05)
        # e06 = self.e06(m06)
        # e07 = self.e05(m07)

        return e01
        # return (e01 + e02 + e03 + e04 + e05 + e06 + e07)/7

def weights_init(m):
    if isinstance(m, nn.Conv2d):
        init.kaiming_uniform(m.weight.data)
        # init.kaiming_uniform(m.bias.data)

train_dataset = data_02.SSDataset(0,4000)
train_loader = DataLoader(train_dataset,batch_size=128,shuffle=True)

val_dataset = data_02.SSDataset(4000,4700)
val_loader = DataLoader(val_dataset,batch_size=128,shuffle=True)

test_dataset = data_02.SSDataset(4700,6700)
test_loader = DataLoader(test_dataset,batch_size=128,shuffle=True)

model = Model()
model.apply(weights_init)
model = model.cuda()



criterion = torch.nn.CrossEntropyLoss(weight=None, size_average=True,ignore_index=0)
# optimizer = torch.optim.SGD(model.parameters(),lr=0.01,momentum=0.99)
optimizer = torch.optim.Adam(model.parameters(),lr=0.003,weight_decay=0.00001)
# optimizer = torch.optim.RMSprop(model.parameters())

for t in range(100):
    running_loss = 0.0
    classification_accuracy = 0.0

    model.train()
    for x_batch,y_batch in train_loader:
        
        # x_batch, y_batch = data()
        # print(x_batch.size())
        # x,y = Variable(x_batch), Variable(y_batch, requires_grad=False)
        x,y = Variable(x_batch).cuda(), Variable(y_batch, requires_grad=False).cuda()

        y_pred = model(x)
        # print(y_pred.size)
        
        loss = criterion(y_pred,y)

        mask = y.data > 0
        y_classes = y.data.index(mask)
        # print(mask)
        # print(y_classes)

        pred = y_pred.data.max(1)[1]
        pred_classes = pred.index(mask)
        # print(pred_classes)
        
        correct = pred_classes.eq(y_classes).cpu().sum()


        optimizer.zero_grad()
        loss.backward()
        optimizer.step()
        # print(t, loss.data[0])
        running_loss += loss.data[0]
        classification_accuracy += correct/y_classes.size()[0]
        print('[%d] loss: %.3f accuracy: %.3f' %
            (t + 1, running_loss, classification_accuracy))
        running_loss = 0.0
        classification_accuracy = 0.0


    val_loss = 0.0
    val_class_acc = 0.0
    model.eval()
    i = 0
    for x_batch,y_batch in val_loader:
        x,y = Variable(x_batch).cuda(), Variable(y_batch, requires_grad=False).cuda()
        y_pred = model(x)
        loss = criterion(y_pred,y)

        mask = y.data > 0
        y_classes = y.data.index(mask)
        # print(mask)
        # print(y_classes)

        pred = y_pred.data.max(1)[1]
        pred_classes = pred.index(mask)
        # print(pred_classes)
        
        correct = pred_classes.eq(y_classes).cpu().sum()

        val_loss += loss.data[0]
        val_class_acc += correct/y_classes.size()[0]
        i += 1
    print("Validation loss: %.3f accuracy: %.3f" % (val_loss/i, val_class_acc/i))


test_loss = 0.0
test_class_acc = 0.0
model.eval()

i = 0
for x_batch,y_batch in test_loader:
    x,y = Variable(x_batch).cuda(), Variable(y_batch, requires_grad=False).cuda()
    y_pred = model(x)
    # print(y_pred)
    loss = criterion(y_pred,y)

    mask = y.data > 0
    y_classes = y.data.index(mask)
    # print(mask)
    # print(y_classes)

    pred = y_pred.data.max(1)[1]
    pred_classes = pred.index(mask)
    # print(pred_classes)
        
    correct = pred_classes.eq(y_classes).cpu().sum()


    test_loss += loss.data[0]
    test_class_acc += correct/y_classes.size()[0]
    i += 1
print("TEST loss: %.3f accuracy: %.3f" % (test_loss/i, test_class_acc/i))

