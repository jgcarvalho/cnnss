import numpy as np
from glob import glob
import os
from torch.utils.data import Dataset
import torch

class SSDataset(Dataset):
    def __init__(self,low,high):
        self.features, self.labels = gen_dataset(low,high)
    
    def __getitem__(self,index):
        return self.features[index], self.labels[index] 
    
    def __len__(self):
        return len(self.features)


# aa symbols: '>' start aa sequence (n-terminal),
#             '<' end aa sequence (c-terminal) 
AA = ['>', 'A', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'K', 'L',
      'M', 'N', 'P', 'Q', 'R', 'S', 'T', 'V', 'Y', 'W', '<']

# ss symbols: '?' undefined or none ss, this symbol will be 
# ignored by loss function
SS = ['C', 'H', 'E','?']

# PAD = 1

INPUT_CODE = len(AA)
LABEL_CODE = len(SS)

MAX_LENGTH = 2000

# ROOT_DIR = "/home/jgcarvalho/RCNN-SS/cntk/data/chamel_data/"
ROOT_DIR = "/home/jgcarvalho/cnnss/data/all50_data/"
SEQ_DIR = ROOT_DIR + "/seq/"
DSSP_DIR = ROOT_DIR + "/dssp/"
STRIDE_DIR = ROOT_DIR + "/stride/"
KAKSI_DIR = ROOT_DIR + "/kaksi/"
PROSS_DIR = ROOT_DIR + "/pross/"
ALL3_DIR = ROOT_DIR + "/all3/"

def encode_aa(seq):
    code = np.zeros((INPUT_CODE, MAX_LENGTH, 1))
    code[AA.index('>'), 0, 0] = 1
    code[AA.index('<'), len(seq)+1, 0] = 1

    for i, aa in enumerate(seq):
        code[AA.index(aa), i+1, 0] = 1

    # for i in range(len(seq)+1, MAX_LENGTH):
    #     code[AA.index('<'), i, 0] = 1

    return torch.FloatTensor(code)

def encode_ss(seq):
    code = np.ones((MAX_LENGTH, 1), dtype=np.int) * SS.index('?')
    for i, ss in enumerate(seq):
        code[i+1, 0] = SS.index(ss)

    return torch.LongTensor(code)

def read_file(fname,ss=False):
    f_ = open(fname)
    f_.readline()
    seq = f_.readline()
    # print(seq)
    f_.close()
    if ss:
        return encode_ss(seq)
    else:
        return encode_aa(seq)

def read_ss(fname):
    return read_file(fname,ss=True)

def read_aa(fname):
    return read_file(fname,ss=False)

def gen_dataset(low,high):
    inputs = []
    labels = []

    file_names = [os.path.basename(x) for x in glob(SEQ_DIR + "*")][low:high]
    # print(file_names)

    # for fn in file_names:
    #     x = read_aa(SEQ_DIR + fn)
    #     inputs.extend([x,x,x,x])


    #     labels.extend([read_ss(DSSP_DIR + fn),
    #     read_ss(STRIDE_DIR + fn),
    #     read_ss(KAKSI_DIR + fn)
    #     ,read_ss(PROSS_DIR + fn)])
        

    #     # print_data_cntk(inputs,labels, masks)
    #     # print_data_cntk(inputs,labels)

    for fn in file_names:
        inputs.append(read_aa(SEQ_DIR + fn))
        labels.append(read_ss(ALL3_DIR + fn))
    
    return inputs,labels


def main():
    print(encode_aa("ACDE"))
    print(encode_aa("ACDE").shape)
    print(encode_ss("HHEECC??"))
    print(encode_ss("HHEECC??").shape)
    # dataset = gen_dataset(0,3)
    # print(dataset[0][0].shape)
    # print(dataset[1][0].shape)
    # print_data_cntk(dataset[0], dataset[1])

if __name__ == '__main__':
    main()
