from random import random
import json

# def print_json(training, validation, testing):
#     print('training')
#     for i in 

with open('/home/jgcarvalho/c35p95_rmMissingPDB_rmSeqError') as f:
    lines = f.readlines()
    pdbs = [x.strip() for x in lines]
    conf = {}
    conf['path'] = "/home/jgcarvalho/local_data/cnnss/processed_data/"
    conf['label'] = ["ss_cons_3_label","asaI_label"]
    conf['training'] = []
    conf['validation'] = []
    conf['testing'] = []
    # training = []
    # validation = []
    # testing = []
    for p in pdbs:
        rnd = random()
        if rnd < 0.7:
            conf['training'].append({'id':p[:4], 'chain':p[4]})
        elif rnd < 0.7 + 0.05:
            conf['validation'].append({'id':p[:4], 'chain':p[4]})
        else:
            conf['testing'].append({'id':p[:4], 'chain':p[4]})
    print(len(pdbs))
    print(len(conf['training']))
    print(len(conf['validation']))
    print(len(conf['testing']))
    # js = json.loads(conf)
    print(json.dumps(conf, sort_keys=True,indent=4, separators=(',', ': ')))


