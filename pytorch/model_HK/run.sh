for HN in {8,16,32,64,128}; do
    echo n $HN
    for M in {1,2,3}; do 
        python model_HK.py ${HN} all3 > log_${HN}_all3_0${M}; 
        python model_HK.py ${HN} dssp > log_${HN}_dssp_0${M}; 
        python model_HK.py ${HN} stride > log_${HN}_stride_0${M}; 
        python model_HK.py ${HN} kaksi > log_${HN}_kaksi_0${M}; 
        python model_HK.py ${HN} pross > log_${HN}_pross_0${M};
    done;
done
