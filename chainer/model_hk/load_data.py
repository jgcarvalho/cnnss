from glob import glob
import os
import numpy
from chainer import dataset

class SSDataset(dataset.DatasetMixin):
    def __init__(self, root):
        self.root_inputs = root+'/inputs/'
        self.labels = glob(root+'/labels/*')

    def __len__(self):
        return len(self.labels)

    def get_example(self, index):
        path_label = self.labels[index] 
        bn = os.path.basename(path_label)
        path_input = self.root_inputs + bn
        # return path_input, path_label
        return numpy.load(path_input), numpy.load(path_label)