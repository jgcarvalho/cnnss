import numpy as np
from glob import glob
import os
from chainer import dataset
import cupy as cp
import json
import pandas as pd
from enum import Enum

class AA(Enum):
    before = 0
    A = 1
    C = 2
    D = 3
    E = 4
    F = 5
    G = 6
    H = 7
    I = 8
    K = 9
    L = 10
    M = 11
    N = 12
    P = 13
    Q = 14
    R = 15
    S = 16
    T = 17
    V = 18
    Y = 19
    W = 20
    after = 21


MAX_LENGTH = 2000
NTERM_PAD = 50
CTERM_PAD = 50
INPUT_CODE = 22

class SSDataset(dataset.DatasetMixin):
    def __init__(self, conf, use="training"):
        if use not in ('training', 'validation', 'testing'):
            print("'use' must be training, validation or testing")
            raise
        # self.use = use
        with open(conf, 'r') as json_file:
            jf = json.load(json_file)

            self.path = jf['path']
            self.tasks = jf['label']
            self.examples = jf[use]
            
            json_file.close()


    def get_example(self, index): 
        ex = self.examples[index]
        # read dataframe
        df = pd.read_feather(self.path + ex['id'] + '.fth')
        # only use the specific chain 
        df = df.loc[df['res_chain'] == ex['chain']]

        # BUG this part should test for errors in sequence like AA=='?'
        # but is not working perfectly
        try:
            input_data = [self._encode_aa(df['seq_res'].values)]
        except ValueError as e:
            print(ex)
            print(e.value)
            raise

        # labels for all specified tasks
        # output_data = [self._pad_label(df[x].values) for x in self.tasks]
        output_data = []
        for t in self.tasks:
            if 'label' in t:
                output_data.append(self._pad_label(df[t].values))
            else:
                output_data.append(self._pad_value(df[t].values))


        # TODO check returned format 
        return tuple(input_data + output_data)


    def get_tasks(self):
        return self.tasks

    def __len__(self):
        return len(self.examples)

    def _encode_aa(self, seq):
        code = np.zeros((INPUT_CODE, MAX_LENGTH+NTERM_PAD+CTERM_PAD, 1), dtype=np.float32)

        for i in range(NTERM_PAD):
            code[AA['before'].value, i, 0] = 1

        for i, aa in enumerate(seq):
            code[AA[aa].value, i+NTERM_PAD, 0] = 1
        
        for i in range(NTERM_PAD+len(seq), len(code)):
            code[AA['after'].value, i, 0] = 1

        return code

    def _pad_label(self, y):
        code = np.ones((MAX_LENGTH+NTERM_PAD+CTERM_PAD, 1), dtype=np.int) * -1 #-1 is the code to ignore 
        
        for i, v in enumerate(y):
            code[i+NTERM_PAD, 0] = v

        return code

    def _pad_value(self, y):
        # code = np.empty((1, MAX_LENGTH+NTERM_PAD+CTERM_PAD, 1), dtype=np.float32) * np.nan
        code = np.empty((1, MAX_LENGTH+NTERM_PAD+CTERM_PAD, 1), dtype=np.float32) * np.nan

        for i, v in enumerate(y):
            code[0, i+NTERM_PAD, 0] = v

        return code
   


def main():
    conf = "./data_mt_test.json"
    db = SSDataset(conf, use="training")
    print(db.path)
    print(db.tasks)
    print(len(db))
    print(db.get_example(1))


if __name__ == '__main__':
    main()
