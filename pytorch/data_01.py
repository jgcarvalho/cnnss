import numpy as np
from glob import glob
import os
from torch.utils.data import Dataset
import torch

class SSDataset(Dataset):
    def __init__(self,low,high):
        self.features, self.labels = gen_dataset(low,high)
    
    def __getitem__(self,index):
        return self.features[index], self.labels[index]
    
    def __len__(self):
        return len(self.features)


AA = ['#','A', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'K', 'L',
      'M', 'N', 'P', 'Q', 'R', 'S', 'T', 'V', 'Y', 'W']

SS = ['C','H', 'E', '?']

PAD = 1

INPUT_CODE = len(AA)
LABEL_CODE = len(SS)

# ROOT_DIR = "/home/jgcarvalho/RCNN-SS/cntk/data/chamel_data/"
ROOT_DIR = "/home/jgcarvalho/RCNN-SS/cntk/data/all50_data/"
SEQ_DIR = ROOT_DIR + "/seq/"
DSSP_DIR = ROOT_DIR + "/dssp/"
STRIDE_DIR = ROOT_DIR + "/stride/"
KAKSI_DIR = ROOT_DIR + "/kaksi/"
PROSS_DIR = ROOT_DIR + "/pross/"

def encode_aa(seq):
    n = 2*PAD+len(seq)
    code = np.ones((INPUT_CODE,n,1)) * (-1.0/(len(AA)-1))
    for i in range(n):
        if i < PAD or i >= PAD+len(seq):
            code[AA.index('#'),i,0] = 1
        else:
            code[AA.index(seq[i-PAD]),i,0] = 1
    return torch.FloatTensor(code)

def encode_ss(seq):
    n = len(seq)
    code = np.zeros((n,1),dtype=np.int)
    for i in range(n):
        code[i,0] = SS.index(seq[i])    
    return torch.LongTensor(code)



def read_file(fname,ss=False):
    f_ = open(fname)
    f_.readline()
    seq = f_.readline()
    # print(seq)
    f_.close()
    if ss:
        return encode_ss(seq)
    else:
        return encode_aa(seq)

def read_ss(fname):
    return read_file(fname,ss=True)

def read_aa(fname):
    return read_file(fname,ss=False)

def gen_dataset(low,high):
    inputs = []
    labels = []

    file_names = [os.path.basename(x) for x in glob(SEQ_DIR + "*")][low:high]
    # print(file_names)

    for fn in file_names:
        x = read_aa(SEQ_DIR + fn)
        inputs.extend([x,x,x,x])


        labels.extend([read_ss(DSSP_DIR + fn),
        read_ss(STRIDE_DIR + fn),
        read_ss(KAKSI_DIR + fn)
        ,read_ss(PROSS_DIR + fn)])
        

        # print_data_cntk(inputs,labels, masks)
        # print_data_cntk(inputs,labels)


        
    return inputs,labels

# def print_data_cntk(inputs, labels, masks):
# # def print_data_cntk(inputs, labels):
#     for i in range(len(inputs)):
#         # c = 1
#         print "|features",
#         for m in range(len(inputs[i])): 
#             for n in range(len(inputs[i][m])):
#                 print " {}".format(inputs[i][m][n]),
#         # for m in range(len(inputs[i])): 
#         #     for n in range(len(inputs[i][m])):
#         #         if inputs[i][m][n] == 1.0:
#         #             print " {}:{}".format(c,inputs[i][m][n]),
#         #         c += 1
#         print "|labels",
#         for m in range(len(labels[i])): 
#             for n in range(len(labels[i][m])):
#                 print " {}".format(labels[i][m][n]),
#         # print "|masks",
#         # for m in range(len(masks[i])): 
#         #     for n in range(len(masks[i][m])):
#         #         print " {}".format(masks[i][m][n]),
#         print "|masks {}".format(masks[i])
#         # print    
        
            


def main():
    # print(encode_aa("ACDE"))
    # print(encode_aa("ACDE").shape)
    # print(encode_ss("HHEECC??"))
    # print(encode_ss("HHEECC??").shape)
    dataset = gen_dataset(0,3)
    print(dataset[0][0].shape)
    print(dataset[1][0].shape)
    # print_data_cntk(dataset[0], dataset[1])

if __name__ == '__main__':
    main()
