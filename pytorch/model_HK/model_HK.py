import numpy as np
import random
import torch 
from torch.autograd import Variable
from torch import nn
from torch.nn import init
from torch.utils.data import TensorDataset, DataLoader
from torch.optim import lr_scheduler
from sys import path, argv 
path.append('../')
import load_data


HIDDEN = int(argv[1])
TRAIN_DB = argv[2]
FILE_MODEL = './param_HK_64.pth'

class Model(nn.Module):
    def __init__(self):
        super(Model,self).__init__()
        self.conv01 = nn.Conv2d(22, HIDDEN, (17,1), padding=(8,0))
        self.sigmoid01 = nn.Sigmoid()
        self.conv02 = nn.Conv2d(HIDDEN,3,(1,1), padding=(0,0))

    def forward(self,x):
        x = self.conv01(x)
        x = self.sigmoid01(x)
        x = self.conv02(x)
        return x

def weights_init(m):
    if isinstance(m, nn.Conv2d):
        init.xavier_normal(m.weight.data)
        # init.kaiming_normal(m.bias.data)

train_dataset = load_data.SSDataset('/tmp/data/train', TRAIN_DB)
train_loader = DataLoader(train_dataset,batch_size=256,shuffle=True, num_workers=4)

val_dataset = load_data.SSDataset('/tmp/data/validation', TRAIN_DB)
val_loader = DataLoader(val_dataset,batch_size=64,shuffle=True)

test_all3_dataset = load_data.SSDataset('/tmp/data/test', 'all3')
test_all3_loader = DataLoader(test_all3_dataset,batch_size=64,shuffle=True)

test_dssp_dataset = load_data.SSDataset('/tmp/data/test', 'dssp')
test_dssp_loader = DataLoader(test_dssp_dataset,batch_size=64,shuffle=True)

test_stride_dataset = load_data.SSDataset('/tmp/data/test', 'stride')
test_stride_loader = DataLoader(test_stride_dataset,batch_size=64,shuffle=True)

test_kaksi_dataset = load_data.SSDataset('/tmp/data/test', 'kaksi')
test_kaksi_loader = DataLoader(test_kaksi_dataset,batch_size=64,shuffle=True)

test_pross_dataset = load_data.SSDataset('/tmp/data/test', 'pross')
test_pross_loader = DataLoader(test_pross_dataset,batch_size=64,shuffle=True)

model = Model()
model.apply(weights_init)
model = model.cuda()


# w = torch.FloatTensor([1, 1, 1.8]).cuda()
# criterion = torch.nn.CrossEntropyLoss(weight=w, size_average=True,ignore_index=3)
criterion = torch.nn.CrossEntropyLoss(size_average=True,ignore_index=3)
# criterion = torch.nn.NLLLoss2d(weight=w, size_average=True,ignore_index=3)
# optimizer = torch.optim.SGD(model.parameters(),lr=0.01,momentum=0.99,nesterov=True)
optimizer = torch.optim.Adam(model.parameters(), lr=0.001)
# optimizer = torch.optim.RMSprop(model.parameters(), lr=0.001)

# scheduler = lr_scheduler.StepLR(optimizer, step_size=50, gamma=0.5)
# scheduler = lr_scheduler.ReduceLROnPlateau(optimizer, 'min', factor=0.5)


for t in range(1000):
    running_loss = 0.0
    classification_accuracy = 0.0
    q_c = 0.0
    q_h = 0.0
    q_e = 0.0
    i = 0
    # scheduler.step()

    model.train()
    for x_batch,y_batch in train_loader:
        
        # x_batch, y_batch = data()
        # print(x_batch.size())
        # x,y = Variable(x_batch), Variable(y_batch, requires_grad=False)
        x,y = Variable(x_batch).cuda(), Variable(y_batch, requires_grad=False).cuda()

        y_pred = model(x)
        # print(y_pred.size)
        
        loss = criterion(y_pred,y)

        mask = y.data < 3
        y_classes = y.data.index(mask)
        # print(mask)
        # print(y_classes)

        pred = y_pred.data.max(1)[1]
        pred_classes = pred.index(mask)
        # print(pred_classes)
        
        correct = pred_classes.eq(y_classes).cpu().sum()

        mask_c = y.data == 0
        mask_h = y.data == 1
        mask_e = y.data == 2
        y_c = y.data.index(mask_c)
        y_h = y.data.index(mask_h)
        y_e = y.data.index(mask_e)
        pred_c = pred.index(mask_c)
        pred_h = pred.index(mask_h)
        pred_e = pred.index(mask_e)
        correct_c = pred_c.eq(y_c).cpu().sum()
        correct_h = pred_h.eq(y_h).cpu().sum()
        correct_e = pred_e.eq(y_e).cpu().sum()
    


        optimizer.zero_grad()
        loss.backward()
        optimizer.step()
        # print(t, loss.data[0])
        running_loss += loss.data[0]
        classification_accuracy += correct/y_classes.size()[0]
        q_c += correct_c/y_c.size()[0]
        q_h += correct_h/y_h.size()[0]
        q_e += correct_e/y_e.size()[0]
        # print('[%d] loss: %.3f accuracy: %.3f' %
            # (t + 1, running_loss, classification_accuracy))
        i += 1
    print('[%d] Training loss: %.3f accuracy (Q3): %.3f Qc: %.3f Qh: %.3f Qe: %.3f' %
            (t + 1, running_loss/i, classification_accuracy/i, q_c/i, q_h/i, q_e/i))    



    val_loss = 0.0
    val_class_acc = 0.0
    q_c = 0.0
    q_h = 0.0
    q_e = 0.0
    model.eval()
    i = 0
    for x_batch,y_batch in val_loader:
        x,y = Variable(x_batch).cuda(), Variable(y_batch, requires_grad=False).cuda()
        y_pred = model(x)
        loss = criterion(y_pred,y)

        mask = y.data < 3
        y_classes = y.data.index(mask)
        # print(mask)
        # print(y_classes)

        pred = y_pred.data.max(1)[1]
        pred_classes = pred.index(mask)
        # print(pred_classes)
        
        correct = pred_classes.eq(y_classes).cpu().sum()
        
        mask_c = y.data == 0
        mask_h = y.data == 1
        mask_e = y.data == 2
        y_c = y.data.index(mask_c)
        y_h = y.data.index(mask_h)
        y_e = y.data.index(mask_e)
        pred_c = pred.index(mask_c)
        pred_h = pred.index(mask_h)
        pred_e = pred.index(mask_e)
        correct_c = pred_c.eq(y_c).cpu().sum()
        correct_h = pred_h.eq(y_h).cpu().sum()
        correct_e = pred_e.eq(y_e).cpu().sum()

        q_c += correct_c/y_c.size()[0]
        q_h += correct_h/y_h.size()[0]
        q_e += correct_e/y_e.size()[0]

        val_loss += loss.data[0]
        val_class_acc += correct/y_classes.size()[0]
        i += 1
    print("[%d] Validation loss: %.3f accuracy: %.3f Qc: %.3f Qh: %.3f Qe: %.3f" % (t+1, val_loss/i, val_class_acc/i, q_c/i, q_h/i, q_e/i))
    # scheduler.step(val_loss)


# TEST 
def test(test_loader):
    test_loss = 0.0
    test_class_acc = 0.0
    q_c = 0.0
    q_h = 0.0
    q_e = 0.0
    model.eval()

    i = 0
    for x_batch,y_batch in test_loader:
        x,y = Variable(x_batch).cuda(), Variable(y_batch, requires_grad=False).cuda()
        y_pred = model(x)
        # print(y_pred)
        loss = criterion(y_pred,y)

        mask = y.data < 3
        y_classes = y.data.index(mask)
        # print(mask)
        # print(y_classes)

        pred = y_pred.data.max(1)[1]
        pred_classes = pred.index(mask)
        # print(pred_classes)
            
        correct = pred_classes.eq(y_classes).cpu().sum()

        mask_c = y.data == 0
        mask_h = y.data == 1
        mask_e = y.data == 2
        y_c = y.data.index(mask_c)
        y_h = y.data.index(mask_h)
        y_e = y.data.index(mask_e)
        pred_c = pred.index(mask_c)
        pred_h = pred.index(mask_h)
        pred_e = pred.index(mask_e)
        correct_c = pred_c.eq(y_c).cpu().sum()
        correct_h = pred_h.eq(y_h).cpu().sum()
        correct_e = pred_e.eq(y_e).cpu().sum()

        q_c += correct_c/y_c.size()[0]
        q_h += correct_h/y_h.size()[0]
        q_e += correct_e/y_e.size()[0]


        test_loss += loss.data[0]
        test_class_acc += correct/y_classes.size()[0]
        i += 1
    print("TEST loss: %.3f accuracy: %.3f Qc: %.3f Qh: %.3f Qe: %.3f" % (test_loss/i, test_class_acc/i, q_c/i, q_h/i, q_e/i))

print('TEST ALL3')
test(test_all3_loader)
print('TEST DSSP')
test(test_dssp_loader)
print('TEST STRIDE')
test(test_stride_loader)
print('TEST KAKSI')
test(test_kaksi_loader)
print('TEST PROSS')
test(test_pross_loader)

torch.save(model.state_dict(), FILE_MODEL)