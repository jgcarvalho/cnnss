import numpy as np
import cupy as cp
import pickle
from chainer import Chain
from chainer import links, initializers, functions, optimizers, training, iterators, report
from chainer import using_config, computational_graph
from chainer import Sequential
from chainer.training import extensions
from chainer import serializers
from chainer import cuda
from sys import path, argv 
import data_mt as data
import pandas as pd

DATA_CONF = './data.json'

TRAIN_BATCH_SIZE = 16
VAL_BATCH_SIZE = 8
TEST_BATCH_SIZE = 8

CHL = 24
N_BLOCKS = 21
gpu_id = 0
initw = initializers.GlorotNormal()

class FeatLayer(Chain):
    def __init__(self):
        super(FeatLayer, self).__init__()
        with self.init_scope():
            self.conv = links.Convolution2D(22, CHL, ksize=(1,1), nobias=True, initialW=initw)
    
    def __call__(self, x):
        return self.conv(x)

class BlockLayer(Chain):
    def __init__(self):
        super(BlockLayer, self).__init__()
        with self.init_scope():
            self.relu01 = links.PReLU(shape=(CHL))
            self.conv01 = links.Convolution2D(CHL, CHL, ksize=(3,1), pad=(1,0), initialW=initw)
            self.relu02 = links.PReLU(shape=(CHL))
            # self.drop 
            self.conv02 = links.Convolution2D(CHL, CHL, ksize=(3,1), pad=(1,0), initialW=initw)

    def __call__(self, x):
        residual = x
        out = self.relu01(x)
        out = self.conv01(out)
        out = self.relu02(out)
        # with using_config('train', True):
        #     out = functions.dropout(out, ratio=0.1)
        out = self.conv02(out)
        return out + residual

class ResNet(Chain):
    def __init__(self, n_blocks):
        super(ResNet, self).__init__()
        with self.init_scope():
            self.feat = FeatLayer()
            self.blocks = self._make_blocks(n_blocks)
            # self.exit = ExitLayer()

    def _make_blocks(self, n_blocks):
        blocks = [BlockLayer() for i in range(n_blocks)]
        return Sequential(*blocks)
        # return blocks
    
    def __call__(self, x):
        x = self.feat(x)

        x = self.blocks(x)
        # b = Sequential(*self.blocks)
        # x = b(x)
        return x

    def predict(self, x, nblocks_to_remove=0):
        x = self.feat(x)
        for i in range(nblocks_to_remove):
            self.blocks.pop()
        print("Number of blocks {}".format(len(self.blocks)))
        x = self.blocks(x)
        # b = Sequential(self.blocks[:nb])
        # x = b(x)
        return x


class ExitLayer(Chain):
    def __init__(self, model, outs):
        super(ExitLayer, self).__init__()
        with self.init_scope():
            self.m = model
            self.conv = links.Convolution2D(CHL, outs, ksize=(1,1), initialW=initw)


    def __call__(self, x):
        x = self.m(x)
        return self.conv(x)

    def predict(self, x, nblocks_to_remove=0):
        x = self.m.predict(x, nblocks_to_remove)
        return functions.softmax(self.conv(x))

class ExitRegLayer(Chain):
    def __init__(self, model, outs):
        super(ExitLayer, self).__init__()
        with self.init_scope():
            self.m = model
            self.conv = links.Convolution2D(CHL, outs, ksize=(1,1), initialW=initw)


    def __call__(self, x):
        x = self.m(x)
        return self.conv(x)


class MultiTaskClassifier(Chain):
    def __init__(self, tasks, losses, acces, lambda_=1.0):
        super(MultiTaskClassifier, self).__init__()
        with self.init_scope():
            
            self.losses = losses
            self.acces = acces
            # BUG chainer issue: can't pass a list of tasks (models) because of scope
            # both commented examples below don't work because of this. the (ugly) solution 
            # was use a for loop

            # self.tasks = tasks
            # self.tasks = [self.task0, self.task1, self.task2, self.task3, self.task4]

            self.tasks = []
            for i, t in enumerate(tasks):
                if i == 0:
                    self.task0 = t
                    self.tasks.append(self.task0)
                if i == 1:
                    self.task1 = t
                    self.tasks.append(self.task1)
                if i == 2:
                    self.task2 = t
                    self.tasks.append(self.task2)
                if i == 3:
                    self.task3 = t
                    self.tasks.append(self.task3)
                if i == 4:
                    self.task4 = t
                    self.tasks.append(self.task4)

    def __call__(self, *args):
        ys = [ t(args[0]) for t in self.tasks ]
        ls = [ self.losses[i](ys[i], args[i+1]) for i in range(len(ys))]
        accs = [ self.acces[i](ys[i], args[i+1]) for i in range(len(ys))]
        
        # total_loss = ls[0] + ls[1] + ls[2] + ls[3] + ls[4]
        total_loss = sum(ls)

        metrics = {'LOSS': total_loss}
        for i in range(len(ls)):
            metrics['loss'+str(1+i)] = ls[i]
            metrics['acc'+str(1+i)] = accs[i]
        report(metrics, self)

        return total_loss


def loss(x, t):
    return functions.softmax_cross_entropy(x, t, ignore_label=-1)   

def accuracy(x, t):
    return functions.accuracy(x, t, ignore_label=-1)   

#TODO loss and accuracy to regression
def mse(x, t):
    mask = cp.isfinite(t)
    x_pred = x[mask]
    x_true = t[mask] * 10
    return functions.mean_squared_error(x_pred, x_true)

def mae(x, t):
    mask = cp.isfinite(t)
    x_pred = x[mask]
    x_true = t[mask] * 10
    return functions.mean_absolute_error(x_pred, x_true) 

def r2(x, t):
    mask = cp.isfinite(t)
    x_pred = x[mask]
    x_true = t[mask] * 10
    return functions.r2_score(x_pred, x_true)



def train_and_val(gpu_id):
    # data and iterators
    traindb = data.SSDataset(DATA_CONF, use="training")
    train_iter = iterators.SerialIterator(traindb, TRAIN_BATCH_SIZE)
    # train_iter = iterators.MultiprocessIterator(traindb, 32)
    valdb = data.SSDataset(DATA_CONF, use="validation")
    val_iter = iterators.SerialIterator(valdb, VAL_BATCH_SIZE, repeat=False, shuffle=False)
    # val_iter = iterators.MultiprocessIterator(valdb, 5, repeat=False, shuffle=False)

    tasks_to_train = traindb.get_tasks()

    model = ResNet(N_BLOCKS)

    tasks = []
    losses = []
    accuracies = []
    for t in tasks_to_train:
        if t == "ss_cons_3_label":
            print("ok")
            task_ss3 = ExitLayer(model, 3)
            tasks.append(task_ss3)
            losses.append(loss)
            accuracies.append(accuracy)
        elif t == "ss_cons_4_label":
            task_ss4 = ExitLayer(model, 4)
            tasks.append(task_ss4)
            losses.append(loss)
            accuracies.append(accuracy)
        elif t == "ss_cons_8_label":
            task_ss8 = ExitLayer(model, 8)
            tasks.append(task_ss8)
            losses.append(loss)
            accuracies.append(accuracy)
        elif t == "asaI_label":
            task_asa4 = ExitLayer(model, 4)
            tasks.append(task_asa4)
            losses.append(loss)
            accuracies.append(accuracy)
        elif t == "phipsi_label":
            task_phipsi576 = ExitLayer(model, 576)
            tasks.append(task_phipsi576)
            losses.append(loss)
            accuracies.append(accuracy)
        elif t == "asaI_rel":
            task_asaIrel = ExitLayer(model, 1)
            tasks.append(task_asaIrel)
            losses.append(mse)
            accuracies.append(r2)
        else:
            print("unknown task:", t)

    # log_trigger = 1, 'iteration'
    log_trigger = 1, 'epoch'
    # classifier = links.Classifier(task1, lossfun=loss, accfun=accuracy)
    classifier = MultiTaskClassifier(tasks, losses, accuracies)


    
    
    # weight decay optimal "w_norm=0.05" (published)
    # w = w_norm*sqrt(b/BT)
    # ex: w = 0.05*sqrt(64/8000*1000)
    # optimizer = optimizers.Adam(alpha=0.001, beta1=0.99, beta2=0.999, weight_decay_rate=0.001)
    # testing alternative optimizers
    optimizer = optimizers.Adam(alpha=0.003)
    # optimizer = optimizers.MomentumSGD(lr=0.0001)
    # optimizer = optimizers.RMSprop(lr=0.003)
    optimizer.setup(classifier)


    updater = training.updaters.StandardUpdater(train_iter, optimizer, device=gpu_id)
    trainer = training.Trainer(updater, stop_trigger=(10, 'epoch'), out='out')
    trainer.extend(extensions.Evaluator(val_iter, classifier, device=gpu_id))
    # trainer.extend(extensions.LogReport(trigger=log_trigger))
    # trainer.extend(training.extensions.dump_graph('main/loss'))
    trainer.extend(training.extensions.Evaluator(val_iter, classifier, device=gpu_id))
    trainer.extend(training.extensions.PrintReport(
        ['epoch', 'iteration','main/LOSS', 'validation/main/LOSS', 'main/loss1', 'main/acc1', 'main/loss2', 'main/acc2', 'main/loss3', 'main/acc3', 'main/loss4', 'main/acc4', 'main/loss5', 'main/acc5']), trigger=log_trigger)

    # Write a log of evaluation statistics for each epoch
    trainer.extend(extensions.LogReport())

    # Save two plot images to the result dir
    if extensions.PlotReport.available():
        trainer.extend(
            extensions.PlotReport(['main/LOSS', 'validation/main/LOSS'],
                                'epoch', file_name='LOSS.png'))
        trainer.extend(
            extensions.PlotReport(
                ['main/loss1', 'validation/main/loss1', 'main/loss2', 'validation/main/loss2', 'main/loss3', 'validation/main/loss3', 'main/loss4', 'validation/main/loss4', 'main/loss5', 'validation/main/loss5'],
                'epoch', file_name='losses.png'))

        trainer.extend(
            extensions.PlotReport(
                ['main/acc1', 'validation/main/acc1', 'main/acc2', 'validation/main/acc2', 'main/acc3', 'validation/main/acc3', 'main/acc4', 'validation/main/acc4', 'main/acc5', 'validation/main/acc5'],
                'epoch', file_name='accuracies.png'))

    trainer.run()

    nets = {'resnet':model}
    for t in enumerate(tasks_to_train):
        nets[t[1]] = tasks[t[0]]

    return nets
    

# def display_graph():
#     g = computational_graph.build_computational_graph(model)
#     print(g)

# display_graph()

def test(model, gpu_id):
    valdb = data.SSDataset(DATA_CONF, use="validation")
    val_iter = iterators.SerialIterator(valdb, TEST_BATCH_SIZE, repeat=False, shuffle=False)
    model.predictor()

def save_models(models, fn_prefix):
    # serializers.save_npz(fn, model)
    for k in models: 
        with open(fn_prefix + k + '.model', 'wb') as f:
            pickle.dump(models[k], f)
            f.close()


def load_model(fn):
    with open(fn, 'rb') as f:
        return pickle.load(f)

def _encode_aa(seq):
    code = cp.zeros((1,data.INPUT_CODE, len(seq)+data.NTERM_PAD+data.CTERM_PAD, 1), dtype=np.float32)

    for i in range(data.NTERM_PAD):
        code[0,data.AA['before'].value, i, 0] = 1

    for i, aa in enumerate(seq):
        code[0,data.AA[aa].value, i+data.NTERM_PAD, 0] = 1
    
    for i in range(data.NTERM_PAD+len(seq), len(code)):
        code[0,data.AA['after'].value, i, 0] = 1

    return code

def inference(model, seq, nb_to_remove=0):
    data_in = _encode_aa(seq)
    data_out = model.predict(data_in,nblocks_to_remove=nb_to_remove)
    
    # print(data_out.shape)
    # H E C -> 0 1 2
    d =  cuda.to_cpu(cp.transpose(data_out[0,:,data.NTERM_PAD:-data.CTERM_PAD,0].data))
    df = pd.DataFrame(d, index=list(seq))

    return df.reset_index()

def predict_ss3(model, seq):
    data = inference(model, seq)
    data.columns = ['Res', 'H', 'E', 'C']
    ss = data.iloc[:,1:].idxmax(axis=1)
    data['SS3'] = ss
    return data

def predict_ss4(model, seq):
    data = inference(model, seq)
    data.columns = ['Res', 'H', 'E', 'C', 'T']
    ss = data.iloc[:,1:].idxmax(axis=1)
    data['SS4'] = ss
    return data


def predict_ss4(model, seq):
    data = inference(model, seq)
    data.columns = ['Res', 'H', 'G', 'I', 'E', 'C', 'T', 'B', 'S']
    ss = data.iloc[:,1:].idxmax(axis=1)
    data['SS4'] = ss
    return data

def predict_sas(model, seq):
    data = inference(model, seq)
    # data.columns = ['Res', 'H', 'G', 'I', 'E', 'C', 'T', 'B', 'S']
    # ss = data.iloc[:,1:].idxmax(axis=1)
    # data['SS4'] = ss
    return data

def main():
    # TRAIN AND VALIDATION
    models = train_and_val(gpu_id)
    # save_models(models, './teste_')
    fn_prefix = argv[1]
    save_models(models, fn_prefix+'_')

    # model = load_model('./teste_ss_cons_3_label.model')

    # save values at the end
    # model = load_model('./teste_resnet.model')
    # result = inference(model, 'MSLQLRSRAAFKLEFLLDRYRVVRKGDAVIEIGSSPGGWTQVLNSLARKIISIDLQEMEEIAGVRFIRCDIFKET')
    # result.to_csv(('./teste_result.csv'))

    # predictor = load_model('./teste_ss_cons_3_label.model')
    # ss = predict_ss3(predictor, 'MSLQLRSRAAFKLEFLLDRYRVVRKGDAVIEIGSSPGGWTQVLNSLARKIISIDLQEMEEIAGVRFIRCDIFKET')
    # print(ss)

    # predictor = load_model('./teste_asaI_rel.model')
    # ss = predict_sas(predictor, 'MSLQLRSRAAFKLEFLLDRYRVVRKGDAVIEIGSSPGGWTQVLNSLARKIISIDLQEMEEIAGVRFIRCDIFKET')
    # print(ss)

if __name__ == '__main__':
    main()
