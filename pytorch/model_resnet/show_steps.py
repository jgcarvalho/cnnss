from sys import argv



header = """
<!DOCTYPE html>
<html>
<head>
<style>
table, th, td {
    border: 1px solid black;
    border-collapse: collapse;
    font-family: "Courier New";
}
th, td {
    padding: 0px;
    text-align: center;    
}
.h {
	background: red;
}
.e {
	background: yellow;
}
.c {
	background: green;
}
.u {
	background: grey;
}
.r {
	font-weight: bold;
}
.
</style>
</head>
<body>
"""

footer = """
</body>
</html>
"""
print(header)
# c = 0
with open(argv[1]) as f:
    pdbid = f.readline()
    seq = f.readline()
    pred = f.readline()
    true = f.readline()
    print('<table>')
    for i in [seq,pred,true]:
        print('<tr>')
        for j in i:
            if j == '*':
                print("<td class='h'>{}</td>".format(j))
            elif j == '|':
                print("<td class='e'>{}</td>".format(j))
            elif j == '-':
                print("<td class='c'>{}</td>".format(j))
            elif j == '?':
                print("<td class='u'>{}</td>".format(j))
            else:
                print("<td class='r'>{}</td>".format(j))
        print('</tr>')
    print('</table>')
    print('<br>')
    print('<table>')
    steps = f.readlines()
    for i in steps:
        print('<tr>')
        for j in i:
            if j == '*':
                print("<td class='h'>{}</td>".format(j))
            elif j == '|':
                print("<td class='e'>{}</td>".format(j))
            elif j == '-':
                print("<td class='c'>{}</td>".format(j))
            elif j == '?':
                print("<td class='u'>{}</td>".format(j))
            else:
                print("<td class='r'>{}</td>".format(j))

        print('</tr>')
    print('</table>')
print(footer)