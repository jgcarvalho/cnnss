import numpy as np
from glob import glob
import os
from chainer import dataset
import cupy as cp
# from torch.utils.data import Dataset
# import torch

class SSDataset(dataset.DatasetMixin):
    def __init__(self, root, src):
        a = ['dssp', 'stride', 'kaksi', 'pross', 'all3']
        if src in a: 
            self.root_inputs = root+'/inputs/'
            self.labels = glob(root+'/labels_'+src+'/*')
        else:
            print("SRC options are 'dssp', 'stride', 'kaksi', 'pross', all3'")

    def get_example(self, index):
        path_label = self.labels[index] 
        bn = os.path.basename(path_label)
        path_input = self.root_inputs + bn
        # return path_input, path_label
        # return torch.load(path_input), torch.load(path_label)
        return np.load(path_input), np.load(path_label)
        # return cp.asarray(np.load(path_input)), cp.asarray(np.load(path_label))
    
   
    def __len__(self):
        return len(self.labels)

def main():
    db = SSDataset('./data/train', 'all3')
    print(db.labels)
    for i in db:
        print(i)


if __name__ == '__main__':
    main()
