import numpy as np
import random
import torch 
from torch.autograd import Variable
from torch import nn
from torch.nn import init
from torch.utils.data import TensorDataset, DataLoader
from torch.optim import lr_scheduler
from sys import path, argv 
path.append('../')
import load_data

N_LAYERS = 21
CHL = 16
TEST_DB = 'all3'
FILE_MODEL = './resnet_21_16.pth'
STOP = int(argv[1])

AA = ['>', 'A', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'K', 'L',
      'M', 'N', 'P', 'Q', 'R', 'S', 'T', 'V', 'Y', 'W', '<']
SS = ['C', 'H', 'E', '?']

test_dataset = load_data.SSDataset('/tmp/data/test', TEST_DB)
test_loader = DataLoader(test_dataset,batch_size=1,shuffle=True)


class FeatModule(nn.Module):
    def __init__(self):
        super(FeatModule, self).__init__()
        self.conv = nn.Conv2d(22,CHL,(1,1), bias=False)

    def forward(self,x):
        return self.conv(x)

class ConvModule(nn.Module):
    def __init__(self):
        super(ConvModule, self).__init__()
        self.relu01 = nn.PReLU(CHL)
        self.conv01 = nn.Conv2d(CHL,CHL,(3,1), padding=(1,0))
        self.relu02 = nn.PReLU(CHL)
        self.drop = nn.Dropout2d(p=0.3)
        self.conv02 = nn.Conv2d(CHL,CHL,(3,1), padding=(1,0))
        
    def forward(self,x):
        residual = x
        out = self.relu01(x)
        out = self.conv01(out)
        out = self.relu02(out)
        out = self.drop(out)
        out = self.conv02(out)
        
        return out + residual
        
class ExitModule(nn.Module):
    def __init__(self):
        super(ExitModule, self).__init__()
        self.conv = nn.Conv2d(CHL,3,(1,1), padding=(0,0))
        
    def forward(self,x):
        return self.conv(x)

class Model(nn.Module):
    def __init__(self, n_layers):
        super(Model,self).__init__()
        self.feat = FeatModule()
        self.conv_layers = self._make_conv_layers(n_layers)
        self.exit = ExitModule()
    
    def _make_conv_layers(self, n_layers):
        layers = [ConvModule() for i in range(n_layers)]
        return nn.Sequential(*layers)
        

    def forward(self,x):
        x = self.feat(x)
        modulelist = list(self.conv_layers.children())
        # self.conv_layers.children
        # print(modulelist)
        # print(len(modulelist))

        for l in modulelist[:STOP]:
            x = l(x)

        x = self.exit(x)
        return x

def weights_init(m):
    if isinstance(m, nn.Conv2d):
        init.xavier_normal(m.weight.data)

model = Model(N_LAYERS)
model = model.cuda()
model.load_state_dict(torch.load(FILE_MODEL))
model.eval()

criterion = torch.nn.CrossEntropyLoss( size_average=True,ignore_index=3)
# optimizer = torch.optim.Adam(model.parameters(), lr=0.001, betas=(0.99, 0.999))

# for t in range(1000):
#     test_loss = 0.0
#     test_class_acc = 0.0
#     i = 0
#     model.train()
#     for x_batch,y_batch in train_loader:
#         x,y = Variable(x_batch).cuda(), Variable(y_batch, requires_grad=False).cuda()
#         y_pred = model(x)
        
#         loss = criterion(y_pred,y)

#         mask = y.data < 3
#         y_classes = y.data.index(mask)
#         pred = y_pred.data.max(1)[1]
#         pred_classes = pred.index(mask)
#         correct = pred_classes.eq(y_classes).cpu().sum()

#         optimizer.zero_grad()
#         loss.backward()
#         optimizer.step()
 
#         test_loss += loss.data[0]
#         test_class_acc += correct/y_classes.size()[0]
#         i += 1
#     print('[%d] Testing loss: %.3f accuracy: %.3f' %
#             (t + 1, test_loss/i, test_class_acc/i))

#     val_loss = 0.0
#     val_class_acc = 0.0
#     q_c = 0.0
#     q_h = 0.0
#     q_e = 0.0
#     model.eval()
#     i = 0
#     for x_batch,y_batch in val_loader:
#         x,y = Variable(x_batch).cuda(), Variable(y_batch, requires_grad=False).cuda()
#         y_pred = model(x)
#         loss = criterion(y_pred,y)

#         mask = y.data < 3
#         y_classes = y.data.index(mask)
#         pred = y_pred.data.max(1)[1]
#         pred_classes = pred.index(mask)
#         correct = pred_classes.eq(y_classes).cpu().sum()
        
#         mask_c = y.data == 0
#         mask_h = y.data == 1
#         mask_e = y.data == 2
#         y_c = y.data.index(mask_c)
#         y_h = y.data.index(mask_h)
#         y_e = y.data.index(mask_e)
#         pred_c = pred.index(mask_c)
#         pred_h = pred.index(mask_h)
#         pred_e = pred.index(mask_e)
#         correct_c = pred_c.eq(y_c).cpu().sum()
#         correct_h = pred_h.eq(y_h).cpu().sum()
#         correct_e = pred_e.eq(y_e).cpu().sum()
#         q_c += correct_c/y_c.size()[0]
#         q_h += correct_h/y_h.size()[0]
#         q_e += correct_e/y_e.size()[0]

#         val_loss += loss.data[0]
#         val_class_acc += correct/y_classes.size()[0]
#         i += 1
#     print("[%d] Validation loss: %.3f accuracy: %.3f Qc: %.3f Qh: %.3f Qe: %.3f" % (t+1, val_loss/i, val_class_acc/i, q_c/i, q_h/i, q_e/i))

def test(test_loader):
    test_loss = 0.0
    test_class_acc = 0.0
    q_c = 0.0
    q_h = 0.0
    q_e = 0.0
    model.eval()

    i = 0
    for x_batch,y_batch in test_loader:
        x,y = Variable(x_batch).cuda(), Variable(y_batch, requires_grad=False).cuda()
        y_pred = model(x)
        loss = criterion(y_pred,y)

        mask = y.data < 3
        y_classes = y.data.index(mask)
        pred = y_pred.data.max(1)[1]
        pred_classes = pred.index(mask)
        correct = pred_classes.eq(y_classes).cpu().sum()

        mask_c = y.data == 0
        mask_h = y.data == 1
        mask_e = y.data == 2
        y_c = y.data.index(mask_c)
        y_h = y.data.index(mask_h)
        y_e = y.data.index(mask_e)
        pred_c = pred.index(mask_c)
        pred_h = pred.index(mask_h)
        pred_e = pred.index(mask_e)
        correct_c = pred_c.eq(y_c).cpu().sum()
        correct_h = pred_h.eq(y_h).cpu().sum()
        correct_e = pred_e.eq(y_e).cpu().sum()

        q_c += correct_c/y_c.size()[0]
        q_h += correct_h/y_h.size()[0]
        q_e += correct_e/y_e.size()[0]


        test_loss += loss.data[0]
        test_class_acc += correct/y_classes.size()[0]
        i += 1
    print("TEST loss: %.3f accuracy: %.3f Qc: %.3f Qh: %.3f Qe: %.3f" % (test_loss/i, test_class_acc/i, q_c/i, q_h/i, q_e/i))

def apply(test_loader):
    for x_batch,y_batch in test_loader:
        x,y = Variable(x_batch).cuda(), Variable(y_batch, requires_grad=False).cuda()
        y_pred = model(x)

        _, idx_seq = torch.max(x.cpu(), 1)
        idx_ss = y.cpu()
        _, idx_pred = torch.max(y_pred.cpu(),1)
        seq = list(idx_seq.data.numpy().flatten())
        ss = list(idx_ss.data.numpy().flatten())
        pred = list(idx_pred.data.numpy().flatten())
        
        # print(x)
        # print(y_pred)
        # _, idx = torch.max(y_pred.cpu(),1)
        # index = 
        # res = ""
        seq_str = ""
        ss_str = ""
        pred_str = ""
        for i in range(len(seq)):
            if seq[i] != 0 and seq[i] != 21:
                seq_str += AA[seq[i]]
                ss_str += SS[ss[i]]
                pred_str += SS[pred[i]]
        print(seq_str)
        print(pred_str)
        print(ss_str)
        
        # print(list(ss.flatten()))
        # print(list(pred.flatten()))
            # print(SS[i])
        # print(indices)

print('TEST ALL3')
apply(test_loader)

print('TEST ALL3')
# test(test_loader)
# print('TEST DSSP')
# test(test_dssp_loader)
# print('TEST STRIDE')
# test(test_stride_loader)
# print('TEST KAKSI')
# test(test_kaksi_loader)
# print('TEST PROSS')
# test(test_pross_loader)

# torch.save(model.state_dict(), FILE_MODEL)
