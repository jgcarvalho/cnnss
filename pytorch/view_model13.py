import torch
import matplotlib.pyplot as plt
import seaborn
import numpy as np

params = torch.load("./model_state_13.pth")

feat = params['feat.conv.weight'].cpu().numpy()


# for i in params.keys():
#     print(i)
x = np.transpose(feat[:,1:21,0,0])
x_norm = (x - x.min(0)) / x.ptp(0)
seaborn.clustermap(feat[:,1:21,0,0], row_cluster=True, method='average', metric='seuclidean')
plt.show()