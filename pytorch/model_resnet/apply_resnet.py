import numpy as np
import random
import torch 
from torch.autograd import Variable
from torch import nn
from torch.nn import init
from torch.nn.functional import softmax
from torch.utils.data import TensorDataset, DataLoader
from torch.optim import lr_scheduler
from sys import path, argv 
path.append('../')
import load_data

N_LAYERS = 21
CHL = 16
TEST_DB = 'all3'
FILE_MODEL = './resnet_21_16.pth'
# STOP = int(argv[1])

AA = ['>', 'A', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'K', 'L',
      'M', 'N', 'P', 'Q', 'R', 'S', 'T', 'V', 'Y', 'W', '<']
SS = ['C', 'H', 'E', '?']

INPUT_CODE = len(AA)
LABEL_CODE = len(SS)

MAX_LENGTH = 3000
NTERM_PAD = 500
CTERM_PAD = 500

# SEQ = "KKAVINGEQIRSISDLHQTLKKELALPEYYGENLDALWDALTGWVEYPLVLEWRQFEQSKQLTENGAESVLQVFREAKAEGADITIILS"
# SEQ = "GDVEKGKKIFVQKCAQCHTVEKGGKHKTGPNLHGLFGRKTGQAPGFTYTDANKNKGITWKEETLMEYLENPKKYIPGTKMIFAGIKKKTEREDLIAYLKKATNE"
# SEQ = "SHMMVNPTVFFDIAVDGEPLGRVSFELFADKVPKTAENFRALSTGEKGFGYKGSCFHRIIPGFMCQGGDFTRHNGTGGKSIYGEKFEDENFILKHTGPGILSMANAGPNTNGSQFFICTAKTEWLDGKHVVFGKVKEGMNIVEAMERFGSRNGKTSKKITIADCGQLE"
SEQ = "DIVLTQSPSSLSASLGDTITITCHASQNINVWLSWYQQKPGNIPKLLIYKASNLHTGVPSRFSGSGSGTGFTLTISSLQPEDIATYYCQQGQSYPLTFGGGTKLEIKRADAAPTVSIFPPSSEQLTSGGASVVCFLNNFYPKDINVKWKIDGSERQNGVLNSWTDQDSKDSTYSMSSTLTLTKDEYERHNSYTCEATHKTSTSPIVKSFNRNEC"
SEQ = "MQIFVKTLTGKTITLEVEPSDTIENVKAKIQDKEGIPPDQQRLIFAGKQLEDGRTLSDYNIQKESTLHLVLRLRGG"
SEQ = "GSKEGEYIKLKVIGQDSSEIHFKVKMTTHLKKLKESYAQRQGVPMNSLRFLFEGQRIADNHTPKELGMEEEDVIEVYQEQTGG"
#PPARG
SEQ = "MGETLGDSPIDPESDSFTDTLSANISQEMTMVDTEMPFWPTNFGISSVDLSVMEDHSHSFDIKPFTTVDFSSISTPHYEDIPFTRTDPVVADYKYDLKLQEYQSAIKVEPASPPYYSEKTQLYNKPHEEPSNSLMAIECRVCGDKASGFHYGVHACEGCKGFFRRTIRLKLIYDRCDLNCRIHKKSRNKCQYCRFQKCLAVGMSHNAIRFGRMPQAEKEKLLAEISSDIDQLNPESADLRALAKHLYDSYIKSFPLTKAKARAILTGKTTDKSPFVIYDMNSLMMGEDKIKFKHITPLQEQSKEVAIRIFQGCQFRSVEAVQEITEYAKSIPGFVNLDLNDQVTLLKYGVHEIIYTMLASLMNKDGVLISEGQGFMTREFLKSLRKPFGDFMEPKFEFAVKFNALELDDSDLAIFIAVIILSGDRPGLLNVKPIEDIQDNLLQALELQLKLNHPESSQLFAKLLQKMTDLRQIVTEHVQLLQVIKKTETDMSLHPLLQEIYKDLY"
#CYTC_HORSE
SEQ = "MGDVEKGKKIFVQKCAQCHTVEKGGKHKTGPNLHGLFGRKTGQAPGFTYTDANKNKGITWKEETLMEYLENPKKYIPGTKMIFAGIKKKTEREDLIAYLKKATNE"
#RNAse H
SEQ = "MLKQVEIFTDGSALGNPGPGGYGAILRYRGREKTFSAGYTRTTNNRMELMAAIVALEALKEHAEVILSTDSQYVRQGITQWIHNWKKRGWKTADKKPVKNVDLWQRLDAALGQHQIKWEWVKGHAGHPENERADELARAAAMNPTLEDTGYQVEV"
#2rldD
#SEQ="GMREDMKDNVVKDKSLEFAVRIVNLYKFLVNEQKEFVMSKQILRSGTSIGANIREAEQAQSRADFINKLNIALKEANETEYWLELLIRTEYITREQYESINNDSTEINKLLISIIKTTKNN"
#galr2
#SEQ = "MNVSGCPGAGNASQAGGGGGWHPEAVIVPLLFALIFLVGTVGNTLVLAVLLRGGQAVSTTNLFILNLGVADLCFILCCVPFQATIYTLDGWVFGSLLCKAVHFLIFLTMHASSFTLAAVSLDRYLAIRYPLHSRELRTPRNALAAIGLIWGLSLLFSGPYLSYYRQSQLANLTVCHPAWSAPRRRAMDICTFVFSYLLPVLVLGLTYARTLRYLWRAVDPVAAGSGARRAKRKVTRMILIVAALFCLCWMPHHALILCVWFGQFPLTRATYALRILSHLVSYANSCVNPIVYALVSKHFRKGFRTICAGLLGRAPGRASGRVCAAARGTHSGSVLERESSDLLHMSEAAGALRPCPGASQPCILEPCPGPSWQGPKAGDSILTVDVA"
#ubiquitin
#SEQ="MQIFVKTLTGKTITLEVEPSDTIENVKAKIQDKEGIPPDQQRLIFAGKQLEDGRTLSDYNIQKESTLHLVLRLRGG"
#ribonuclease A (1afu)
SEQ = "KETAAAKFERQHMDSSTSAASSSNYCNQMMKSRNLTKDRCKPVNTFVHESLADVQAVCSQKNVACKNGQTNCYQSYSTMSITDCRETGSSKYPNCAYKTTQANKHIIVACEGNPYVPVHFDASV"
# Drosophila Notch ankyrin domain (1ot8)
SEQ = "MGLTPLMIAAVRGGGLDTGEDIENNEDSTAQVISDLLAQGAELNATMDKTGETSLHLAARFARADAAKRLLDAGADANSQDNTGRTPLHAAVAADAMGVFQILLRNRATNLNARMHDGTTPLILAARLAIEGMVEDLITADADINAADNSGKTALHWAAAVNNTEAVNILLMHHANRDAQDDKDETPLFLAAREGSYEASKALLDNFANREITDHMDRLPRDVASERLHHDIVRLLDEH"
# proteina joao
SEQ = "CVDKKLDKQYESLSLFHPSNVEMLSSMDSASVRIIKPFPAPQTPGRLQPAPVIPSAPAAPKLDHQRMDTIQEDPSTDSHMDEDGFEKDPFPNSSTAAKSFEDLTDHPVTRSEKAASFKLQRQNRVDSKETEC"
SEQ = "MRQSLLFLTSVVPFVLAPRPPDDPGFGPHQRLEKLDSLLSDYDILSLSNIQQHSVRKRDLQTSTHVETLLTFSALKRHFKLYLTSSTERFSQNFKVVVVDGKNESEYTVKWQDFFTGHVVGEPDSRVLAHIRDDDVIIRINTDGAEYNIEPLWRFVNDTKDKRMLVYKSEDIKNVSRLQSPKVCGYLKVDNEELLPKGLVDREPPEELVHRVKRRADPDPMKNTCKLLVVADHRFYRYMGRGEESTTTNYLIELIDRVDDIYRNTSWDNAGFKGYGIQIEQIRILKSPQEVKPGEKHYNMAKSYPNEEKDAWDVKMLLEQFSFDIAEEASKVCLAHLFTYQDFDMGTLGLAYVGSPRANSHGGVCPKAYYSPVGKKNIYLNSGLTSTKNYGKTILTKEADLVTTHELGHNFGAEHDPDGLAECAPNEDQGGKYVMYPIAVSGDHENNKMFSNCSKQSIYKTIESKAQECFQERSNKVCGNSRVDEGEECDPGIMYLNNDTCCNSDCTLKEGVQCSDRNSPCCKNCQFETAQKKCQEAINATCKGVSYCTGNSSECPPPGNAEDDTVCLDLGKCKDGKCIPFCEREQQLESCACNETDNSCKVCCRDLSGRCVPYVDAEQKNLFLRKGKPCTVGFCDMNGKCEKRVQDVIERFWDFIDQLSINTFGKFLADNIVGSVLVFSLIFWIPFSILVHCVDKKLDKQYESLSLFHPSNVEMLSSMDSASVRIIKPFPAPQTPGRLQPAPVIPSAPAAPKLDHQRMDTIQEDPSTDSHMDEDGFEKDPFPNSSTAAKSFEDLTDHPVTRSEKAASFKLQRQNRVDSKETEC"
# 2fuk
#SEQ = "MSNPLFPTESAALTLDGPVGPLDVAVDLPEPDVAVQPVTAIVCHPLSTEGGSMHNKVVTMAARALRELGITVVRFNFRSVGTSAGSFDHGDGEQDDLRAVAEWVRAQRPTDTLWLAGFSFGAYVSLRAAAALEPQVLISIAPPAGRWDFSDVQPPAQWLVIQGDADEIVDPQAVYDWLETLEQQPTLVRMPDTSHFFHRKLIDLRGALQHGVRRWLPATP"


# test_dataset = load_data.SSDataset('/tmp/data/test', TEST_DB)
# test_loader = DataLoader(test_dataset,batch_size=1,shuffle=True)


class FeatModule(nn.Module):
    def __init__(self):
        super(FeatModule, self).__init__()
        self.conv = nn.Conv2d(22,CHL,(1,1), bias=False)

    def forward(self,x):
        return self.conv(x)

class ConvModule(nn.Module):
    def __init__(self):
        super(ConvModule, self).__init__()
        self.relu01 = nn.PReLU(CHL)
        self.conv01 = nn.Conv2d(CHL,CHL,(3,1), padding=(1,0))
        self.relu02 = nn.PReLU(CHL)
        self.drop = nn.Dropout2d(p=0.3)
        self.conv02 = nn.Conv2d(CHL,CHL,(3,1), padding=(1,0))
        
    def forward(self,x):
        residual = x
        out = self.relu01(x)
        out = self.conv01(out)
        out = self.relu02(out)
        out = self.drop(out)
        out = self.conv02(out)
        
        return out + residual
        
class ExitModule(nn.Module):
    def __init__(self):
        super(ExitModule, self).__init__()
        self.conv = nn.Conv2d(CHL,3,(1,1), padding=(0,0))
        
    def forward(self,x):
        return self.conv(x)

class Model(nn.Module):
    def __init__(self, n_layers):
        super(Model,self).__init__()
        self.feat = FeatModule()
        self.conv_layers = self._make_conv_layers(n_layers)
        self.exit = ExitModule()
        self.stop = n_layers
    
    def _make_conv_layers(self, n_layers):
        layers = [ConvModule() for i in range(n_layers)]
        return nn.Sequential(*layers)
        

    def forward(self,x):
        x = self.feat(x)
        modulelist = list(self.conv_layers.children())
        # self.conv_layers.children
        # print(modulelist)
        # print(len(modulelist))

        for l in modulelist[:self.stop]:
            x = l(x)

        x = self.exit(x)
        return x

    def set_stop(self, stop):
        self.stop = stop


def weights_init(m):
    if isinstance(m, nn.Conv2d):
        init.xavier_normal(m.weight.data)

model = Model(N_LAYERS)
model = model.cuda()
model.load_state_dict(torch.load(FILE_MODEL))
model.eval()


def apply(protein):
    init = None
    for stop in range(0, N_LAYERS):
        model.set_stop(stop)
        x = Variable(protein).cuda()
        y_pred = softmax(model(x), dim=1)
        # print(y_pred[0,:,1000,0])
        if stop == 0:
            init = y_pred.cpu()
        #     print(init[0,:,500:510,0])
        # print(y_pred[0,:,500:510,0])
        _, idx_seq = torch.max(x.cpu(), 1)
	# show changes using initial probs as ref
        # _, idx_pred = torch.max(y_pred.cpu() - init,1)
        _, idx_pred = torch.max(y_pred.cpu(),1)
        seq = list(idx_seq.data.numpy().flatten())
        pred = list(idx_pred.data.numpy().flatten())

        seq_str = ""
        pred_str = ""
        length = 0
        for i in range(len(seq)):
            if seq[i] != 0 and seq[i] != 21:
                seq_str += AA[seq[i]]
                pred_str += SS[pred[i]]
                length += 1
        print(pred_str)
        # if stop == N_LAYERS-1:
        #     print(init[0,:,NTERM_PAD:NTERM_PAD+length,0])
        #     print(y_pred[0,:,NTERM_PAD:NTERM_PAD+length,0])



# print('TEST ALL3')
def encode_aa(seq):
    code = np.zeros((1,INPUT_CODE, MAX_LENGTH+NTERM_PAD+CTERM_PAD, 1))
    # code[AA.index('>'), 0, 0] = 1
    # code[AA.index('<'), len(seq)+1, 0] = 1
    for i in range(NTERM_PAD):
        code[0,AA.index('>'), i, 0] = 1

    for i, aa in enumerate(seq):
        code[0,AA.index(aa), i+NTERM_PAD, 0] = 1
    
    for i in range(NTERM_PAD+len(seq), len(code)):
        code[0,AA.index('<'), i, 0] = 1

    return torch.FloatTensor(code)


def main():
    seq = encode_aa(SEQ)
    apply(seq)

if __name__ == '__main__':
    main()


