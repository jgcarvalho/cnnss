import os

with open("../results_21_16") as f:
    f.readline()
    lines = f.readlines()
    
    j = 0
    for i in range(0,len(lines),3):
        out = open("pred_"+str(j), 'w')
        out.write(">AA\n")
        out.write(lines[i])
        out.write(">PSEC\n")
        out.write(lines[i+1])
        out.write(">OSEC\n")
        out.write(lines[i+2])
        j+=1

