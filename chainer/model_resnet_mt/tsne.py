import pandas as pd 
from time import time
from sklearn import manifold
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import offsetbox
from enum import Enum

class AA(Enum):
    before = 0
    A = 1
    C = 2
    D = 3
    E = 4
    F = 5
    G = 6
    H = 7
    I = 8
    K = 9
    L = 10
    M = 11
    N = 12
    P = 13
    Q = 14
    R = 15
    S = 16
    T = 17
    V = 18
    Y = 19
    W = 20
    after = 21

def plot_embedding(X, y, title=None):
    x_min, x_max = np.min(X, 0), np.max(X, 0)
    X = (X - x_min) / (x_max - x_min)
    print(X)

    plt.figure()
    ax = plt.subplot(111)
    for i in range(X.shape[0]):
        # plot aa letter in different colours
        plt.text(X[i, 0], X[i, 1], y[i],
                color=plt.cm.Set1(AA[y[i]].value / 20.),
                fontdict={'weight': 'bold', 'size': 9})


    # if hasattr(offsetbox, 'AnnotationBbox'):
    #     # only print thumbnails with matplotlib > 1.0
    #     shown_images = np.array([[1., 1.]])  # just something big
    #     for i in range(X.shape[0]):
    #         dist = np.sum((X[i] - shown_images) ** 2, 1)
    #         if np.min(dist) < 4e-3:
    #             # don't show points that are too close
    #             continue
    #         shown_images = np.r_[shown_images, [X[i]]]
    #         # imagebox = offsetbox.AnnotationBbox(
    #         #     offsetbox.OffsetImage(digits.images[i], cmap=plt.cm.gray_r),
    #         #     X[i])
    #         ax.add_artist(imagebox)
    plt.xticks([]), plt.yticks([])
    if title is not None:
        plt.title(title)


df = pd.read_csv('./teste_result.csv')
y = df['index'].values
X = df.iloc[:,2:].values

######
print("Computing t-SNE embedding")
tsne = manifold.TSNE(n_components=2, perplexity=50, learning_rate=200, init='pca', random_state=0)
t0 = time()
X_tsne = tsne.fit_transform(X)
print(X_tsne)

plot_embedding(X_tsne, y, 
               "t-SNE embedding of the amino acids (time %.2fs)" %
               (time() - t0))

plt.show()