import torch
import matplotlib.pyplot as plt
import seaborn
import numpy as np

params = torch.load("./resnet_21_32.pth")

print(params.keys())
print(params.len())
feat = params['feat.conv.weight'].cpu().numpy()


# # for i in params.keys():
# #     print(i)
# # x = np.transpose(feat[:,1:21,0,0])
# # x_norm = (((x - x.min(0)) / x.ptp(0))-0.5)*2.0
# # seaborn.clustermap(feat[:,1:21,0,0], row_cluster=True, method='average', metric='euclidean', z_score=0)
# seaborn.clustermap(feat[:,1:21,0,0], z_score=0, method='average', metric='sqeuclidean')
# # seaborn.clustermap(np.transpose(x_norm), row_cluster=True, method='single', metric='euclidean')
# plt.show()
