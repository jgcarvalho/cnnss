from chainer import Chain
from chainer import links, initializers, functions, optimizers, training, iterators
from chainer import using_config, computational_graph
from chainer import Sequential
from chainer.training import extensions
from sys import path, argv 
path.append('../')
import load_data

ROOT_DATA = '../data'
LABEL_FROM = 'all3'

CHL = 16
N_BLOCKS = 21
gpu_id = 0
initw = initializers.GlorotNormal()

class FeatLayer(Chain):
    def __init__(self):
        super(FeatLayer, self).__init__()
        with self.init_scope():
            self.conv = links.Convolution2D(22, CHL, ksize=(1,1), nobias=True, initialW=initw)
    
    def __call__(self, x):
        return self.conv(x)

class BlockLayer(Chain):
    def __init__(self):
        super(BlockLayer, self).__init__()
        with self.init_scope():
            self.relu01 = links.PReLU(shape=(CHL))
            self.conv01 = links.Convolution2D(CHL, CHL, ksize=(3,1), pad=(1,0), initialW=initw)
            self.relu02 = links.PReLU(shape=(CHL))
            # self.drop 
            self.conv02 = links.Convolution2D(CHL, CHL, ksize=(3,1), pad=(1,0), initialW=initw)

    def __call__(self, x):
        residual = x
        out = self.relu01(x)
        out = self.conv01(out)
        out = self.relu02(out)
        with using_config('train', True):
            out = functions.dropout(out, ratio=0.1)
        out = self.conv02(out)
        return out + residual

class ExitLayer(Chain):
    def __init__(self):
        super(ExitLayer, self).__init__()
        with self.init_scope():
            self.conv = links.Convolution2D(CHL, 3, ksize=(1,1), initialW=initw)

    def __call__(self, x):
        return self.conv(x)

class ResNet(Chain):
    def __init__(self, n_blocks):
        super(ResNet, self).__init__()
        with self.init_scope():
            self.feat = FeatLayer()
            self.blocks = self._make_blocks(n_blocks)
            self.exit = ExitLayer()

    def _make_blocks(self, n_blocks):
        blocks = [BlockLayer() for i in range(n_blocks)]
        return Sequential(*blocks)
        # s = Sequential()
        # for i in range(n_layers):
        #   s.append(BlockLayer())
        # return s
    
    def __call__(self, x):
        x = self.feat(x)
        x = self.blocks(x)
        x = self.exit(x)
        return x

def loss(x, t):
    # print(functions.softmax(x),t)
    return functions.softmax_cross_entropy(x, t, ignore_label=3)   

def accuracy(x, t):
    return functions.accuracy(x, t, ignore_label=3)   

def train_and_val(gpu_id):

    model = ResNet(N_BLOCKS)
    # if gpu_id >= 0:
    #     model.to_gpu(gpu_id)

    log_trigger = 1, 'iteration'
    # loss = functions.softmax_cross_entropy
    classifier = links.Classifier(model, lossfun=loss, accfun=accuracy)
    # classifier.to_gpu(0)

    traindb = load_data.SSDataset(ROOT_DATA+'/train', LABEL_FROM)
    train_iter = iterators.SerialIterator(traindb, 24)
    valdb = load_data.SSDataset(ROOT_DATA+'/validation', LABEL_FROM)
    val_iter = iterators.SerialIterator(valdb, 10, repeat=False, shuffle=False)
    
    # weight decay optimal "w_norm=0.05" (published)
    # w = w_norm*sqrt(b/BT)
    # ex: w = 0.05*sqrt(64/8000*1000)
    optimizer = optimizers.Adam(alpha=0.003, beta1=0.99, beta2=0.999, weight_decay_rate=0.001)
    # optimizer = optimizers.MomentumSGD(lr=0.0001)
    # optimizer = optimizers.Adam(alpha=0.0001)
    # optimizer = optimizers.RMSprop(lr=0.0003)
    optimizer.setup(classifier)
    # optimizer.

    updater = training.updaters.StandardUpdater(train_iter, optimizer, device=gpu_id)
    trainer = training.Trainer(updater, stop_trigger=(1000, 'epoch'), out='out')
    trainer.extend(extensions.Evaluator(val_iter, classifier, device=gpu_id))
    trainer.extend(extensions.LogReport(trigger=log_trigger))
    trainer.extend(training.extensions.dump_graph('main/loss'))
    # trainer.extend(training.extensions.Evaluator(val_iter, model, device=0))
    # trainer.extend(training.extensions.LogReport(trigger=log_trigger))
    trainer.extend(training.extensions.PrintReport(
        ['epoch', 'iteration', 'main/loss', 'validation/main/loss',
         'main/accuracy', 'validation/main/accuracy', 'main/lr', 'lr']), trigger=log_trigger)
    trainer.run()
    

# def display_graph():
#     g = computational_graph.build_computational_graph(model)
#     print(g)

# display_graph()


train_and_val(gpu_id)