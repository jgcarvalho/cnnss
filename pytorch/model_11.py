import numpy as np
import random
import torch 
from torch.autograd import Variable
from torch import nn
from torch.nn import init
from torch.utils.data import TensorDataset, DataLoader
from torch.optim import lr_scheduler
import load_data

CHL=32
class FeatModule(nn.Module):
    def __init__(self):
        super(FeatModule, self).__init__()
        self.conv = nn.Conv2d(22,CHL,(1,1))
        # self.relu = nn.PReLU()

    def forward(self,x):
        return self.conv(x)

class ConvModule(nn.Module):
    def __init__(self):
        super(ConvModule, self).__init__()
        # self.bn01 = nn.BatchNorm2d(16)
        self.relu01 = nn.PReLU(CHL)
        self.conv01 = nn.Conv2d(CHL,CHL,(3,1), padding=(1,0))
        # self.bn02 = nn.BatchNorm2d(32)
        self.relu02 = nn.PReLU(CHL)
        # self.pool02 = nn.AvgPool2d((3,1), stride=1, padding=(1,0))
        
        
        self.conv02 = nn.Conv2d(CHL,CHL,(3,1), padding=(1,0))
        # self.relu03 = nn.PReLU()


    def forward(self,x):
        # bn01 = self.bn01(x)
        relu01 = self.relu01(x)
        conv01 = self.conv01(relu01)
        
        # print(conv01.shape())
        
        # bn02 = self.bn02(conv01)
        relu02 = self.relu02(conv01)
        # pool02 = self.pool02(relu01)
        # concat = torch.cat((pool02, relu02), 1)
        conv02 = self.conv02(relu02)
        # relu03 = self.relu03(conv02)
        
        return conv02

# class FullModule(nn.Module):
#     def __init__(self):
#         super(FullModule, self).__init__()
#         self.m_in = ConvModule()
#         self.m_ou = ConvModule()

#     def forward(self,x):
#         return self.m_ou(self.m_in(x))

class ExitModule(nn.Module):
    def __init__(self):
        super(ExitModule, self).__init__()
        self.drop = nn.Dropout2d(p=0.5)
        self.conv = nn.Conv2d(CHL,3,(1,1), padding=(0,0))
        # self.relu = nn.ReLU()

        
    def forward(self,x):
        # return self.relu(self.conv(self.drop(x)))
        # return self.relu(self.conv(x))
        return self.conv(self.drop(x))

class Model(nn.Module):
    def __init__(self):
        super(Model,self).__init__()
        self.feat = FeatModule()
        self.m01 = ConvModule()
        # self.e01 = ExitModule()
        self.m02 = ConvModule()
        # self.e02 = ExitModule()
        self.m03 = ConvModule()
        # self.e03 = ExitModule()
        self.m04 = ConvModule()
        # self.e04 = ExitModule()
        self.m05 = ConvModule()
        # self.e05 = ExitModule()
        self.m06 = ConvModule()
        # self.e06 = ExitModule()
        self.m07 = ConvModule()
        # self.e07 = ExitModule()
        self.m08 = ConvModule()
        self.m09 = ConvModule()
        self.m10 = ConvModule()
        self.m11 = ConvModule()
        # self.m12 = ConvModule()
        # self.m13 = ConvModule()
        # self.m14 = ConvModule()
        # self.m15 = ConvModule()
        # self.m16 = ConvModule()
        # self.m17 = ConvModule()
        # self.m18 = ConvModule()
        # self.m19 = ConvModule()
        # self.m20 = ConvModule()
        # self.m21 = ConvModule()
        # self.m22 = ConvModule()
        # self.m23 = ConvModule()
        # self.m24 = ConvModule()
        # self.m25 = ConvModule()
        # self.m26 = ConvModule()
        # self.m27 = ConvModule()
        # self.m28 = ConvModule()
        # self.m29 = ConvModule()
        # self.m30 = ConvModule()
        # self.m31 = ConvModule()
        self.e31 = ExitModule()
        # self.relu = nn.ReLU()
        self.soft = nn.Softmax2d()

    def forward(self,x):
        feats = self.feat(x)
        m01 = feats + self.m01(feats)
        m02 = m01 + self.m02(m01) 
        m03 = m02 + self.m03(m02)
        m04 = m03 + self.m04(m03)
        m05 = m04 + self.m05(m04)
        m06 = m05 + self.m06(m05)
        m07 = m06 + self.m07(m06)
        m08 = m07 + self.m08(m07)
        m09 = m08 + self.m09(m08)
        m10 = m09 + self.m10(m09)
        m11 = m10 + self.m11(m10)
        # m12 = m11 + self.m12(m11)
        # m13 = m12 + self.m13(m12)
        # m14 = m13 + self.m14(m13)
        # m15 = m14 + self.m15(m14)
        # m16 = m15 + self.m16(m15)
        # m17 = m16 + self.m17(m16)
        # m18 = m17 + self.m18(m17)
        # m19 = m18 + self.m19(m18)
        # m20 = m19 + self.m20(m19)
        # m21 = m20 + self.m21(m20)
        # m22 = m21 + self.m22(m21)
        # m23 = m22 + self.m23(m22)
        # m24 = m23 + self.m24(m23)
        # m25 = m24 + self.m25(m24)
        # m26 = m25 + self.m26(m25)
        # m27 = m26 + self.m27(m26)
        # m28 = m27 + self.m28(m27)
        # m29 = m28 + self.m29(m28)
        # m30 = m29 + self.m30(m29)
        # m31 = m30 + self.m31(m30)
        # exit nodes
        # e01 = self.e01(m01)
        # e02 = self.e02(m02)
        # e03 = self.e03(m03)
        # e04 = self.e04(m04)
        # e05 = self.e05(m05)
        # e06 = self.e06(m06)
        # e07 = self.e07(m07)
        e31 = self.e31(m11)

        # return (e01 + e02 + e03)/3
        # return (e01 + e02 + e03 + e04 + e05 + e06 + e07)/7
        # return self.soft(e31)
        return e31

def weights_init(m):
    if isinstance(m, nn.Conv2d):
        init.xavier_normal(m.weight.data)
        # init.kaiming_normal(m.bias.data)

# train_dataset = data_02.SSDataset(0,5000)
# train_loader = DataLoader(train_dataset,batch_size=64,shuffle=True)

# val_dataset = data_02.SSDataset(5000,5500)
# val_loader = DataLoader(val_dataset,batch_size=64,shuffle=True)

# test_dataset = data_02.SSDataset(5500,6700)
# test_loader = DataLoader(test_dataset,batch_size=64,shuffle=True)

train_dataset = load_data.SSDataset('/tmp/data/train', 'all3')
train_loader = DataLoader(train_dataset,batch_size=64,shuffle=True)

val_dataset = load_data.SSDataset('/tmp/data/validation', 'all3')
val_loader = DataLoader(val_dataset,batch_size=32,shuffle=False)

test_dataset = load_data.SSDataset('/tmp/data/test', 'all3')
test_loader = DataLoader(test_dataset,batch_size=32,shuffle=False)

model = Model()
model.apply(weights_init)
model = model.cuda()


w = torch.FloatTensor([1.3, 1.2, 2]).cuda()
criterion = torch.nn.CrossEntropyLoss(weight=w, size_average=True,ignore_index=3)
# criterion = torch.nn.CrossEntropyLoss( size_average=True,ignore_index=3)
# criterion = torch.nn.NLLLoss2d(weight=w, size_average=True,ignore_index=3)
# optimizer = torch.optim.SGD(model.parameters(),lr=0.01,momentum=0.999,nesterov=True)
optimizer = torch.optim.Adam(model.parameters(), lr=0.001, betas=(0.99, 0.999))
# optimizer = torch.optim.SparseAdam(model.parameters(), lr=0.001)
# optimizer = torch.optim.RMSprop(model.parameters(), lr=0.001)

# scheduler = lr_scheduler.StepLR(optimizer, step_size=50, gamma=0.5)
# scheduler = lr_scheduler.ReduceLROnPlateau(optimizer, 'min', factor=0.5)

for t in range(50):
    running_loss = 0.0
    classification_accuracy = 0.0
    # scheduler.step()

    model.train()
    for x_batch,y_batch in train_loader:
        
        # x_batch, y_batch = data()
        # print(x_batch.size())
        # x,y = Variable(x_batch), Variable(y_batch, requires_grad=False)
        x,y = Variable(x_batch).cuda(), Variable(y_batch, requires_grad=False).cuda()

        y_pred = model(x)
        # print(y_pred.size)
        
        loss = criterion(y_pred,y)

        mask = y.data < 3
        y_classes = y.data.index(mask)
        # print(mask)
        # print(y_classes)

        pred = y_pred.data.max(1)[1]
        pred_classes = pred.index(mask)
        # print(pred_classes)
        
        correct = pred_classes.eq(y_classes).cpu().sum()

        # mask_c = y.data == 0
        # mask_h = y.data == 1
        # mask_e = y.data == 2
        # y_c = y.data.index(mask_c)
        # y_h = y.data.index(mask_h)
        # y_e = y.data.index(mask_e)
        # pred_c = pred.index(mask_c)
        # pred_h = pred.index(mask_h)
        # pred_e = pred.index(mask_e)
        # correct_c = pred_c.eq(y_c).cpu().sum()
        # correct_h = pred_h.eq(y_h).cpu().sum()
        # correct_e = pred_e.eq(y_e).cpu().sum()
    


        optimizer.zero_grad()
        loss.backward()
        optimizer.step()
        # print(t, loss.data[0])
        running_loss = loss.data[0]
        classification_accuracy = correct/y_classes.size()[0]
        # q_c = correct_c/y_c.size()[0]
        # q_h = correct_h/y_h.size()[0]
        # q_e = correct_e/y_e.size()[0]
        print('[%d] loss: %.3f accuracy: %.3f' %
            (t + 1, running_loss, classification_accuracy))
        # print('[%d] loss: %.3f accuracy (Q3): %.3f Qc: %.3f Qh: %.3f Qe: %.3f' %
        #     (t + 1, running_loss, classification_accuracy, q_c, q_h, q_e))    



    val_loss = 0.0
    val_class_acc = 0.0
    q_c = 0.0
    q_h = 0.0
    q_e = 0.0
    model.eval()
    i = 0
    for x_batch,y_batch in val_loader:
        x,y = Variable(x_batch).cuda(), Variable(y_batch, requires_grad=False).cuda()
        y_pred = model(x)
        loss = criterion(y_pred,y)

        mask = y.data < 3
        y_classes = y.data.index(mask)
        # print(mask)
        # print(y_classes)

        pred = y_pred.data.max(1)[1]
        pred_classes = pred.index(mask)
        # print(pred_classes)
        
        correct = pred_classes.eq(y_classes).cpu().sum()
        
        mask_c = y.data == 0
        mask_h = y.data == 1
        mask_e = y.data == 2
        y_c = y.data.index(mask_c)
        y_h = y.data.index(mask_h)
        y_e = y.data.index(mask_e)
        pred_c = pred.index(mask_c)
        pred_h = pred.index(mask_h)
        pred_e = pred.index(mask_e)
        correct_c = pred_c.eq(y_c).cpu().sum()
        correct_h = pred_h.eq(y_h).cpu().sum()
        correct_e = pred_e.eq(y_e).cpu().sum()

        q_c += correct_c/y_c.size()[0]
        q_h += correct_h/y_h.size()[0]
        q_e += correct_e/y_e.size()[0]

        val_loss += loss.data[0]
        val_class_acc += correct/y_classes.size()[0]
        i += 1
    print("Validation loss: %.3f accuracy: %.3f Qc: %.3f Qh: %.3f Qe: %.3f" % (val_loss/i, val_class_acc/i, q_c/i, q_h/i, q_e/i))
    # scheduler.step(val_loss)


test_loss = 0.0
test_class_acc = 0.0
q_c = 0.0
q_h = 0.0
q_e = 0.0
model.eval()

i = 0
for x_batch,y_batch in test_loader:
    x,y = Variable(x_batch).cuda(), Variable(y_batch, requires_grad=False).cuda()
    y_pred = model(x)
    # print(y_pred)
    loss = criterion(y_pred,y)

    mask = y.data < 3
    y_classes = y.data.index(mask)
    # print(mask)
    # print(y_classes)

    pred = y_pred.data.max(1)[1]
    pred_classes = pred.index(mask)
    # print(pred_classes)
        
    correct = pred_classes.eq(y_classes).cpu().sum()

    mask_c = y.data == 0
    mask_h = y.data == 1
    mask_e = y.data == 2
    y_c = y.data.index(mask_c)
    y_h = y.data.index(mask_h)
    y_e = y.data.index(mask_e)
    pred_c = pred.index(mask_c)
    pred_h = pred.index(mask_h)
    pred_e = pred.index(mask_e)
    correct_c = pred_c.eq(y_c).cpu().sum()
    correct_h = pred_h.eq(y_h).cpu().sum()
    correct_e = pred_e.eq(y_e).cpu().sum()

    q_c += correct_c/y_c.size()[0]
    q_h += correct_h/y_h.size()[0]
    q_e += correct_e/y_e.size()[0]


    test_loss += loss.data[0]
    test_class_acc += correct/y_classes.size()[0]
    i += 1
print("TEST loss: %.3f accuracy: %.3f Qc: %.3f Qh: %.3f Qe: %.3f" % (test_loss/i, test_class_acc/i, q_c/i, q_h/i, q_e/i))

